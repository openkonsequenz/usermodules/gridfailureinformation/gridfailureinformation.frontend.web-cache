# Org.Eclipse.OpenkUsermodules.GridFailureInformation.Frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.19.

## Development server

Run `npm run start-integration` for a dev server. Navigate to `http://entopkon:8880/portalFE/#/login`. Try to login with known credentials and open the 'SIT Localhost' app. The app will automatically reload if you change any of the source files.

## Build

Run `npm run build-main-app` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `mpm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Implementation

### Directives

#### VisibleByDependentFieldDirective

This directive is used to hide or show html elemnts depending on defined dependencies (isVisible = true) Additionally it sets the state of depending field to avoid ngrx-forms validation errors.

**Example**

```
<button
  [visibleByDependentField]="formState.value.branch === Globals.BUSINESS_RULE_FIELDS.branch.telecommunication">
</button>
```

#### FormValidatorDirective

This validator fills the validation(visualization) gap of ngrx-forms for disabled/dependent fields

It ist important that `.ngrx-forms-invalid-directive` class is added to global style.scss

#### FormDisableDirective

This directive disables the entire form depending on user rights. It automatically select `form[ngrxFormState]` and checks user rights

### VisibleByRightDirective

This directive shows/hides html elements depending on user rights.

**Example**

```
<button *visibleByRight="[RolesEnum.PUBLISHER]" type="button" class="btn btn-link navbar-btn">
  <fa name="users"></fa>
</button>
```
