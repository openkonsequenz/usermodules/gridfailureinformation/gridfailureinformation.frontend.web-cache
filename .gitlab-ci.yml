variables:
  FF_USE_FASTZIP: "true" # enable fastzip - a faster zip implementation that also supports level configuration.
  ARTIFACT_COMPRESSION_LEVEL: default
  CACHE_COMPRESSION_LEVEL: default
  TRANSFER_METER_FREQUENCY: 5s # will display transfer progress every 5 seconds for artifacts and remote caches.
  SONAR_USER_HOME: ${CI_PROJECT_DIR}/.sonar  # Defines the location of the analysis task cache
  GIT_DEPTH: 0  # Tells git to fetch all the branches of the project, required by the analysis task
  CLI_VERSION: latest
  DOCKER_BUILDKIT: 1 # With BuildKit, you don't need to pull the remote images before building since it caches each build layer in your image registry. Then, when you build the image, each layer is downloaded as needed during the build.

image: node:14

#-----------------------------------------------------------------------------------------------------------------------
stages: # map-library (Build, Test, Docker) als auch create-map-elements werden aktuell nicht berücksichtigt!
  - install-dependencies  # jobs: install-dependencies-main / install-dependencies-web-cache
  - build                 # jobs: build-main / build-map-app / build-table-app / build-web-comp / build-web-cache
  - test                  # jobs: test-main-app / test-map-app / test-table-app / test-web-cache
  - sonarqube             # noch nicht fertig getestet jobs: sonarqube-main / sonarqube-web-cache
  - dockerimage           # jobs: docker-build-main / docker-build-map-app / docker-build-table-app / docker-build-web-comp / docker-build-web-cache

  ########################################################################################################################
  # Non-Parallel Deploy-Stages
  ########################################################################################################################
  - Deploy-Main
  - Deploy-Map-App
  - Deploy-Table-App
  - Deploy-Web-Comp
  - Deploy-Web-Cache

#-----------------------------------------------------------------------------------------------------------------------
install-dependencies-main:
#-----------------------------------------------------------------------------------------------------------------------
  stage: install-dependencies
  cache:
    key: $CI_COMMIT_REF_SLUG-$CI_PROJECT_DIR
    paths:
      - node_modules/
  script:
    - npm ci
  #only:
  #  changes:
  #    - package-lock.json

#-----------------------------------------------------------------------------------------------------------------------
install-dependencies-web-cache:
#-----------------------------------------------------------------------------------------------------------------------
  stage: install-dependencies
  cache:
    key: $CI_COMMIT_REF_SLUG-$CI_PROJECT_DIR-web-cache
    paths:
      - ./projects/grid-failure-information-web-cache/node_modules/
  script:
    - cd ./projects/grid-failure-information-web-cache
    - npm ci
  #only:
  #  changes:
  #    - /projects/grid-failure-information-web-cache/package-lock.json

#-----------------------------------------------------------------------------------------------------------------------
build-main:
#-----------------------------------------------------------------------------------------------------------------------
  stage: build
  dependencies:
    - install-dependencies-main
  cache:
    key: $CI_COMMIT_REF_SLUG-$CI_PROJECT_DIR
    paths:
      - node_modules/
    policy: pull
  script:
    - npm run sy-pre-start
    - npm run build-main-app
    - npm run sy-post-build
    - find -maxdepth 2 -ls
  artifacts:
    paths:
      - dist/

#-----------------------------------------------------------------------------------------------------------------------
build-map-app:
#-----------------------------------------------------------------------------------------------------------------------
  stage: build
#  dependencies:
#    - build-map-library
  cache:
    key: $CI_COMMIT_REF_SLUG-$CI_PROJECT_DIR
    paths:
      - node_modules/
    policy: pull
  script:
    - npm run build-map-app
    - find -maxdepth 2 -ls
  artifacts:
    paths:
      - dist/

#-----------------------------------------------------------------------------------------------------------------------
build-table-app:
#-----------------------------------------------------------------------------------------------------------------------
  stage: build
#  dependencies:
#    - build-map-app
  cache:
    key: $CI_COMMIT_REF_SLUG-$CI_PROJECT_DIR
    paths:
      - node_modules/
    policy: pull
  script:
    - npm run build-table-app
    - find -maxdepth 2 -ls
  artifacts:
    paths:
      - dist/

#-----------------------------------------------------------------------------------------------------------------------
build-web-comp:
#-----------------------------------------------------------------------------------------------------------------------
  stage: build
#  dependencies:
#    - build-table-app
  cache:
    key: $CI_COMMIT_REF_SLUG-$CI_PROJECT_DIR
    paths:
      - node_modules/
    policy: pull
  script:
    - npm run build-comp
  artifacts:
    paths:
      - dist/

#-----------------------------------------------------------------------------------------------------------------------
build-web-cache:
#-----------------------------------------------------------------------------------------------------------------------
  stage: build
  dependencies:
    - install-dependencies-web-cache
#  needs: ["install-dependencies-web-cache"]
  cache:
    key: $CI_COMMIT_REF_SLUG-$CI_PROJECT_DIR-web-cache
    paths:
      - ./projects/grid-failure-information-web-cache/node_modules/
    policy: pull
  script:
    - cd ./projects/grid-failure-information-web-cache
    - npm run build
    - find -maxdepth 2 -ls
  artifacts:
    paths:
      - ./projects/grid-failure-information-web-cache/dist/

#-----------------------------------------------------------------------------------------------------------------------
test-main-app:
#-----------------------------------------------------------------------------------------------------------------------
  stage: test
  image: trion/ng-cli-karma:${CLI_VERSION}
  allow_failure: false
#  dependencies:
#    - install_dependencies
  cache:
    key: $CI_COMMIT_REF_SLUG-$CI_PROJECT_DIR
    paths:
      - node_modules/
    policy: pull
  script:
      - npm run test-single-run --code-coverage --progress false --watch false
  artifacts:
    paths:
      - coverage/

#-----------------------------------------------------------------------------------------------------------------------
test-map-app:
#-----------------------------------------------------------------------------------------------------------------------
  stage: test
  image: trion/ng-cli-karma:${CLI_VERSION}
  allow_failure: false
#  dependencies:
#    - install_dependencies
  cache:
    key: $CI_COMMIT_REF_SLUG-$CI_PROJECT_DIR
    paths:
      - node_modules/
    policy: pull
  script:
    - npm run test-map-app-single-run --code-coverage --progress false --watch false
  artifacts:
    paths:
      - coverage/

#-----------------------------------------------------------------------------------------------------------------------
test-table-app:
#-----------------------------------------------------------------------------------------------------------------------
  stage: test
  image: trion/ng-cli-karma:${CLI_VERSION}
  allow_failure: false
#  dependencies:
#    - install_dependencies
  cache:
    key: $CI_COMMIT_REF_SLUG-$CI_PROJECT_DIR
    paths:
      - node_modules/
    policy: pull
  script:
    - npm run test-table-app-single-run --code-coverage --progress false --watch false
  artifacts:
    paths:
      - coverage/

#-----------------------------------------------------------------------------------------------------------------------
test-web-cache:
#-----------------------------------------------------------------------------------------------------------------------
  stage: test
  image: trion/ng-cli-karma:${CLI_VERSION}
  allow_failure: false
#  dependencies:
#    - install_dependencies-web-cache
  cache:
    key: $CI_COMMIT_REF_SLUG-$CI_PROJECT_DIR-web-cache
    paths:
      - ./projects/grid-failure-information-web-cache/node_modules/
    policy: pull
  script:
    - cd ./projects/grid-failure-information-web-cache
    - npm run test:cov --code-coverage --progress false --watch false
  artifacts:
    paths:
      - ./projects/grid-failure-information-web-cache/coverage/
  rules:
    - changes:
        - stoerungsauskunftInterface/**/*
      if: $CI_PROJECT_NAME == "gridFailureInformation.frontend.web-cache"

#-----------------------------------------------------------------------------------------------------------------------
sonarqube-main:
#-----------------------------------------------------------------------------------------------------------------------
  stage: sonarqube
  image:
    name: sonarsource/sonar-scanner-cli:4.6
    entrypoint: [""]
  cache:
    key: ${CI_JOB_NAME}
    paths:
      - .sonar/cache
  script:
    - echo ${CI_PROJECT_DIR}
    - sonar-scanner -Dsonar.qualitygate.wait=true -Dsonar.branch.name="${CI_COMMIT_REF_NAME}"
  allow_failure: true
  dependencies:
    - test-main-app

#-----------------------------------------------------------------------------------------------------------------------
sonarqube-web-cache:
#-----------------------------------------------------------------------------------------------------------------------
  stage: sonarqube
  image:
    name: sonarsource/sonar-scanner-cli:4.6
    entrypoint: [""]
  cache:
    key: ${CI_JOB_NAME}
    paths:
      - .sonar/cache
  script:
    - echo ${CI_PROJECT_DIR}
    - cd ./projects/grid-failure-information-web-cache
    - sonar-scanner -Dsonar.qualitygate.wait=true -Dsonar.branch.name="${CI_COMMIT_REF_NAME}"
  allow_failure: true
  dependencies:
    - test-web-cache

#-----------------------------------------------------------------------------------------------------------------------
# Dockerimage
#-----------------------------------------------------------------------------------------------------------------------
.docker-build-script:
  image: docker:20.10.7-git
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - echo PROJECT_DIR $PROJECT_DIR
    - echo DOCKER_FILE $DOCKER_FILE
    - ls -l
    - |
      if [[ "$CI_COMMIT_TAG" == "" ]]; then
        tag="sha-${CI_COMMIT_SHORT_SHA}"
      else
        tag="$CI_COMMIT_TAG"
      fi
    - echo current tag ${tag}
    - CI_SOURCE_BRANCH=$(git for-each-ref | grep $CI_COMMIT_SHA | grep origin | sed "s/.*\///" | tr '[:upper:]' '[:lower:]')
    - echo CI_SOURCE_BRANCH $CI_SOURCE_BRANCH
    - REGISTRY_IMAGE_BASE="$CI_REGISTRY_IMAGE/$CI_SOURCE_BRANCH/$IMG_NAME"
    - FINAL_REGISTRY_IMAGE="$CI_REGISTRY_IMAGE/$CI_SOURCE_BRANCH/$IMG_NAME:${tag}"
    - echo "FINAL_REGISTRY_IMAGE=$FINAL_REGISTRY_IMAGE" >> dockerimage.env
    - echo current FINAL_REGISTRY_IMAGE ${FINAL_REGISTRY_IMAGE}
    - docker build --pull -f $DOCKER_FILE -t "$FINAL_REGISTRY_IMAGE" -t "$REGISTRY_IMAGE_BASE" --cache-from "$REGISTRY_IMAGE_BASE"  --build-arg BUILDKIT_INLINE_CACHE=1 .
    - docker push "$REGISTRY_IMAGE_BASE" --all-tags
  artifacts:
    reports:
      dotenv: dockerimage.env


#-----------------------------------------------------------------------------------------------------------------------
docker-build-main:
#-----------------------------------------------------------------------------------------------------------------------
  stage: dockerimage
  extends: .docker-build-script
  variables:
    IMG_NAME: "gfi-main"
    DOCKER_FILE: "./projects/grid-failure-information-app/Dockerfile_Kubernetes_Main"
  needs:
    - job: build-main
  rules:
    - exists:
        - projects/grid-failure-information-app/Dockerfile_Kubernetes_Main

#-----------------------------------------------------------------------------------------------------------------------
docker-build-map-app:
#-----------------------------------------------------------------------------------------------------------------------
  stage: dockerimage
  extends: .docker-build-script
  variables:
    IMG_NAME: "gfi-map-app"
    DOCKER_FILE: "./projects/grid-failure-information-map-app/Dockerfile_Kubernetes_MapApp"
    PROJECT_DIR: "projects/grid-failure-information-map-app"
  needs:
    - job: build-map-app
  rules:
    - exists:
        - projects/grid-failure-information-map-app/Dockerfile_Kubernetes_MapApp

#-----------------------------------------------------------------------------------------------------------------------
docker-build-table-app:
#-----------------------------------------------------------------------------------------------------------------------
  stage: dockerimage
  extends: .docker-build-script
  variables:
    IMG_NAME: "gfi-table-app"
    DOCKER_FILE: "./projects/grid-failure-information-table-app/Dockerfile_Kubernetes_TableApp"
    PROJECT_DIR: "projects/grid-failure-information-table-app"
  needs:
    - job: build-table-app
  rules:
    - exists:
        - projects/grid-failure-information-table-app/Dockerfile_Kubernetes_TableApp

#-----------------------------------------------------------------------------------------------------------------------
docker-build-web-cache:
  #-----------------------------------------------------------------------------------------------------------------------
  stage: dockerimage
  extends: .docker-build-script
  variables:
    IMG_NAME: "gfi-web-cache"
    DOCKER_FILE: "./projects/grid-failure-information-web-cache/Dockerfile_Kubernetes_WebCache"
    PROJECT_DIR: "projects/grid-failure-information-web-cache"
  needs:
    - job: build-web-cache
  rules:
    - exists:
        - projects/grid-failure-information-web-cache/Dockerfile_Kubernetes_WebCache

#-----------------------------------------------------------------------------------------------------------------------
docker-build-web-comp:
#-----------------------------------------------------------------------------------------------------------------------
  stage: dockerimage
  extends: .docker-build-script
  variables:
    IMG_NAME: "gfi-web-comp"
    DOCKER_FILE: "./projects/grid-failure-information-web-comp/Dockerfile_Kubernetes_WebComp"
    PROJECT_DIR: "projects/grid-failure-information-web-comp"
  needs:
    - job: build-web-comp
  rules:
    - exists:
        - projects/grid-failure-information-web-comp/Dockerfile_Kubernetes_WebComp


#-----------------------------------------------------------------------------------------------------------------------
# Deploy
#-----------------------------------------------------------------------------------------------------------------------
.deploy-script:
  image: alpine:3.14.0
  cache: {}
  variables:
    GIT_STRATEGY: none
    DEPLOYMENT_FOLDER: apps/gridfailureinformation/$DEPLOYMENT_SUB_FOLDER
  before_script:
    - apk add --no-cache git curl bash coreutils
    - curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"  | bash
    - mv kustomize /usr/local/bin/
    - ls -l
    - git clone https://${CI_USERNAME}:${CI_PUSH_TOKEN}@gitlab.com/${GITLAB_DEPLOYMENT_REPO_URL}
    - cd *
    - git config --global user.email "gitlab@gitlab.com"
    - git config --global user.name "GitLab CI/CD"
  script:
    - ls -l
    - echo FINAL_REGISTRY_IMAGE ${FINAL_REGISTRY_IMAGE}
    - cd ${DEPLOYMENT_FOLDER}
    - kustomize edit set image ${BASE_IMAGE_NAME}=${FINAL_REGISTRY_IMAGE}
    - cat kustomization.yaml
    - git commit -am '[skip ci] Image update'
    - git push origin main

#------------------------------
# Deploy - QA-Environment
#------------------------------
deploy-qa-main:
  stage: Deploy-Main
  extends: .deploy-script
  variables:
    BASE_IMAGE_NAME: main-image
    DEPLOYMENT_SUB_FOLDER: frontend/environments/qa
  dependencies:
    - docker-build-main
  only:
    - master

deploy-qa-map-app:
  stage: Deploy-Map-App
  extends: .deploy-script
  variables:
    BASE_IMAGE_NAME: main-image
    DEPLOYMENT_SUB_FOLDER: map-app/environments/qa
  dependencies:
    - docker-build-map-app
  only:
    - master

deploy-qa-table-app:
  stage: Deploy-Table-App
  extends: .deploy-script
  variables:
    BASE_IMAGE_NAME: main-image
    DEPLOYMENT_SUB_FOLDER: table-app/environments/qa
  dependencies:
    - docker-build-table-app
  only:
    - master

deploy-qa-web-comp:
  stage: Deploy-Web-Comp
  extends: .deploy-script
  variables:
    BASE_IMAGE_NAME: main-image
    DEPLOYMENT_SUB_FOLDER: web-comp/environments/qa
  dependencies:
    - docker-build-web-comp
  only:
    - master

deploy-qa-web-cache:
  stage: Deploy-Web-Cache
  extends: .deploy-script
  variables:
    BASE_IMAGE_NAME: main-image
    DEPLOYMENT_SUB_FOLDER: web-cache/environments/qa
  dependencies:
    - docker-build-web-cache
  only:
    - master

#------------------------------
# Deploy - DEV-Environment
#------------------------------
deploy-dev-main:
  stage: Deploy-Main
  extends: .deploy-script
  variables:
    BASE_IMAGE_NAME: main-image
    DEPLOYMENT_SUB_FOLDER: frontend/environments/dev
  dependencies:
    - docker-build-main
  only:
    - DEVELOP

deploy-dev-map-app:
  stage: Deploy-Map-App
  extends: .deploy-script
  variables:
    BASE_IMAGE_NAME: main-image
    DEPLOYMENT_SUB_FOLDER: map-app/environments/dev
  dependencies:
    - docker-build-map-app
  only:
    - DEVELOP

deploy-dev-table-app:
  stage: Deploy-Table-App
  extends: .deploy-script
  variables:
    BASE_IMAGE_NAME: main-image
    DEPLOYMENT_SUB_FOLDER: table-app/environments/dev
  dependencies:
    - docker-build-table-app
  only:
    - DEVELOP

deploy-dev-web-comp:
  stage: Deploy-Web-Comp
  extends: .deploy-script
  variables:
    BASE_IMAGE_NAME: main-image
    DEPLOYMENT_SUB_FOLDER: web-comp/environments/dev
  dependencies:
    - docker-build-web-comp
  only:
    - DEVELOP

deploy-dev-web-cache:
  stage: Deploy-Web-Cache
  extends: .deploy-script
  variables:
    BASE_IMAGE_NAME: main-image
    DEPLOYMENT_SUB_FOLDER: web-cache/environments/dev
  dependencies:
    - docker-build-web-cache
  only:
    - DEVELOP
