/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Subject } from 'rxjs';
import { VisibilityConfigurationInterface } from '@openk-libs/grid-failure-information-map/shared/models/visibility-configuration.interface';
import { Globals } from '@openk-libs/grid-failure-information-map/constants/globals';

export class MapOptions {
  public forceResize$: Subject<any> = new Subject<any>();
  public overviewMapInitialZoom: number = Globals.OVERVIEW_MAP_INITIAL_ZOOM_FACTOR;
  public detailMapInitialZoom: number = Globals.DETAIL_MAP_INITIAL_ZOOM_FACTOR;
  public extendMarkerInformation: boolean = false;
  public overviewMapInitialLatitude: string = null;
  public overviewMapInitialLongitude: string = null;
  public visibilityConfiguration: VisibilityConfigurationInterface = null;
  public dataExternInitialVisibility: string = null;

  public constructor(data: any = null) {
    Object.keys(data || {})
      .filter(property => this.hasOwnProperty(property))
      .forEach(property => (this[property] = data[property]));
  }
}
