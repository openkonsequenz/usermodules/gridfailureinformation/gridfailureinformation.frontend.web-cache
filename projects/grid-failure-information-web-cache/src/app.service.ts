/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable, HttpStatus } from '@nestjs/common';
import * as fs from 'fs-extra';

import { Response } from 'express';
@Injectable()
export class AppService {
  saveGridFailures(sits: any, res: Function){
    const jsonString = JSON.stringify(sits);
    return fs.writeFile('./store/public-sits.json', jsonString, err => {
      if (err) {
        throw err;
      } else {
        res(true, 'Successfully wrote file');
      }
    });
  }

  saveSettings(sits: any, res: Function) {
    const jsonString = JSON.stringify(sits);
    return fs.writeFile('./store/public-settings.json', jsonString, err => {
      if (err) {
        throw err;
      } else {
        res(true, 'Successfully wrote file');
      }
    });
  }

  getSITs(res: Function) {
    fs.readFile('./store/public-sits.json', 'utf8', function(err, data) {
      if (err) throw err;
      else res(data); //do operation on data that generates say resultArray;
    });
  }

  getSettings(res: Function) {
    fs.readFile('./store/public-settings.json', 'utf8', function(err, data) {
      if (err) throw err;
      else res(data); //do operation on data that generates say resultArray;
    });
  }
}
