/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { IconCellRendererComponent } from '@grid-failure-information-app/shared/components/cell-renderer/icon-cell-renderer/icon-cell-renderer.component';

export const DISTRIBUTION_GROUP_MEMBER_POSTCODES_COLDEF = [
  {
    field: 'postcode',
    headerName: 'DistributionGroupMemberPostcodes.Postcode',
    sortable: true,
    suppressMovable: true,
    filter: 'setFilterComponent',
  },
  {
    field: 'tools',
    headerName: ' ',
    pinned: 'right',
    maxWidth: 50,
    minWidth: 50,
    lockPosition: true,
    sortable: false,
    filter: false,
    suppressMenu: true,
    suppressSizeToFit: true,
    cellRendererFramework: IconCellRendererComponent,
  },
];
