/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { DistributionGroupSandbox } from '@grid-failure-information-app/app/pages/distribution-group/distribution-group.sandbox';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { BaseList } from '@grid-failure-information-app/shared/components/base-components/base.list';
import { DISTRIBUTION_GROUP_COLDEF } from '@grid-failure-information-app/app/pages/distribution-group/distribution-group-list/distribution-group-list-column-definition';
import { SetFilterComponent } from '@grid-failure-information-app/shared/filters/ag-grid/set-filter/set-filter.component';
import { GridApi } from 'ag-grid-community';
import { RolesEnum, DistributionPublicationStatusEnum } from '@grid-failure-information-app/shared/constants/enums';

@Component({
  selector: 'app-distribution-group-list',
  templateUrl: './distribution-group-list.component.html',
  styleUrls: ['./distribution-group-list.component.scss'],
})
export class DistributionGroupListComponent extends BaseList implements OnInit, OnDestroy {
  public columnDefinition: any = DISTRIBUTION_GROUP_COLDEF;
  public frameworkComponents: { setFilterComponent: any };
  public api: GridApi | null | undefined;
  public RolesEnum = RolesEnum;
  public plzValueId: string;

  constructor(public sandbox: DistributionGroupSandbox) {
    super();
    this.frameworkComponents = { setFilterComponent: SetFilterComponent };
  }

  ngOnInit() {
    this.gridOptions = {
      ...this.gridOptions,
      onModelUpdated: event => {
        this.api = event.api;
        if (event.api.getDisplayedRowAtIndex(0)) {
          if (!!this.sandbox.selectedDistributionGroup) {
            event.api.forEachNode(node => node.setSelected(node.data.id === this.sandbox.selectedDistributionGroup.id));
          } else {
            event.api.getDisplayedRowAtIndex(0).setSelected(true);
          }
        }
      },
      onCellClicked: event => {
        this.sandbox.disableMemberButton = false;
        this.sandbox.showMembersToSelectedGroup(event);
        this.sandbox.loadDistributionGroupDetail(event.data.id);
        this.sandbox.oldSelectedTemplate = DistributionPublicationStatusEnum.PUBLISH;
        this.sandbox.selectedMemberRowIndex = 0;
        this.sandbox.clearAssignmentInput();
      },
    };
    this.gridOptions.context = {
      ...this.gridOptions.context,
      icons: { delete: true },
    };
    this.gridOptions.context.eventSubject.subscribe(event => {
      if (event.type === 'delete') {
        this.sandbox.deleteDistributionGroup(event.data.id);
      }
    });
  }

  public unselectFirstRow() {
    if (this.api && this.api.getDisplayedRowAtIndex(0)) {
      this.api.getDisplayedRowAtIndex(0).setSelected(false);
    }
  }

  ngOnDestroy(): void {
    this.sandbox.endSubscriptions();
  }
}
