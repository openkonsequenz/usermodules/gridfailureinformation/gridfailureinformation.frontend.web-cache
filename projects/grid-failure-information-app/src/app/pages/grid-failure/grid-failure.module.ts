/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { GridFailureSandbox } from '@grid-failure-information-app/pages/grid-failure/grid-failure-list/grid-failure.sandbox';
import { GridFailureRoutingModule } from '@grid-failure-information-app/pages/grid-failure/grid-failure-routing.module';
import { GridFailureListComponent } from '@grid-failure-information-app/pages/grid-failure/grid-failure-list/grid-failure-list.component';
import { GridFailureDetailsComponent } from '@grid-failure-information-app/pages/grid-failure/grid-failure-details/grid-failure-details.component';
import { GridFailureDetailsSandbox } from '@grid-failure-information-app/pages/grid-failure/grid-failure-details/grid-failure-details.sandbox';
import { GridFailureDistributionGroupsComponent } from '@grid-failure-information-app/pages/grid-failure/grid-failure-details/grid-failure-distribution-groups/grid-failure-distribution-groups.component';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { ComponentsModule } from '@grid-failure-information-app/shared/components/components.module';
import { TranslateModule } from '@ngx-translate/core';
import { NgrxFormsModule } from 'ngrx-forms';
import { FormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';
import { DirectivesModule } from '@grid-failure-information-app/shared/directives/directives.module';
import { FiltersModule } from '@grid-failure-information-app/shared/filters/filters.module';
import { ContainersModule } from '@grid-failure-information-app/shared/containers/containers.module';
import { SetFilterComponent } from '@grid-failure-information-app/shared/filters/ag-grid/set-filter/set-filter.component';
import { GridFailureService } from '@grid-failure-information-app/pages/grid-failure/grid-failure.service';
import { GridFailureApiClient } from '@grid-failure-information-app/pages/grid-failure/grid-failure-api-client';
import { EffectsModule } from '@ngrx/effects';
import { GridFailuresEffects } from '@grid-failure-information-app/shared/store/effects/grid-failures.effect';
import { GridFailuresResolver } from '@grid-failure-information-app/pages/grid-failure//grid-failure.resolver';
import { StoreModule } from '@ngrx/store';
import { gridFailureReducers } from '@grid-failure-information-app/shared/store';
import { NgbDatepickerModule, NgbTimepickerModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UtilityModule } from '@grid-failure-information-app/shared/utility';
import { PipesModule } from '@grid-failure-information-app/shared/pipes/pipes.module';
import { GridFailureInformationMapModule } from 'openk/grid-failure-information-map/src/public-api';
import { FuzzySetFilterComponent } from '@grid-failure-information-app/shared/filters/ag-grid/fuzzy-set-filter/fuzzy-set-filter.component';
@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    TranslateModule,
    DirectivesModule,
    FiltersModule,
    ReactiveFormsModule,
    RouterModule,
    NgrxFormsModule,
    FormsModule,
    NgbDatepickerModule,
    NgbTimepickerModule,
    NgbModule,
    UtilityModule,
    PipesModule,
    StoreModule.forFeature('grid-failure', gridFailureReducers),
    AgGridModule.withComponents([SetFilterComponent, FuzzySetFilterComponent]),
    EffectsModule.forFeature([GridFailuresEffects]),
    ContainersModule,
    PipesModule,
    GridFailureRoutingModule,
    GridFailureInformationMapModule,
    NgbModule,
  ],
  declarations: [GridFailureListComponent, GridFailureDetailsComponent, GridFailureDistributionGroupsComponent],
  providers: [GridFailureSandbox, GridFailureDetailsSandbox, GridFailureService, GridFailureApiClient, GridFailuresResolver],
})
export class GridFailureModule {}
