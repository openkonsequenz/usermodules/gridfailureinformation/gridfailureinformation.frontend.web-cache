/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { GridFailureDetailsComponent } from '@grid-failure-information-app/pages/grid-failure/grid-failure-details/grid-failure-details.component';
import { of } from 'rxjs';
import { Store } from '@ngrx/store';
import { State } from '@grid-failure-information-app/shared/store';
import { Globals } from '@grid-failure-information-app/shared/constants/globals';
import { INITIAL_STATE } from '@grid-failure-information-app/shared/store/reducers/grid-failures/grid-failure-details-form.reducer';
import * as models from '@grid-failure-information-app/shared/models';

describe('GridFailureDetailsComponent', () => {
  let component: GridFailureDetailsComponent;
  let gridFailureSandbox: any;
  let actionsSubject: any;
  let gridFailureDetailsFormResponse: any = {
    value: {
      failureBegin: 'test1',
      failureEndPlanned: 'test2',
      failureEndResupplied: 'test3 ',
    },
    controls: {
      postcode: { value: null },
      stationDescription: { value: null },
      latitude: { value: null },
    } as any,
  };
  let appState: Store<State>;

  beforeEach(() => {
    appState = { dispatch: () => {}, pipe: () => of(true), select: () => of(true) } as any;
    actionsSubject = { pipe: () => of(true), map: () => ({}), next: () => ({}) } as any;
    gridFailureSandbox = {
      init() {},
      clearGridFailureData() {},
      registerEvents() {},
      endSubscriptions() {},
      resetCoords() {},
      resetStationId() {},
      resetPostCode() {},
      setViewStateForReqProps() {},
      setSelectedStation() {},
      resetFailureLocationValues() {},
      deleteGridFailureStation() {},
      setBranchState() {},
      setFormStateDirty() {},
      setFormStatePristine() {},
      setFaultLocationArea() {},
      gridFailureDetailsFormState$: of({ ...INITIAL_STATE, gridFailureDetailsFormResponse } as any),
      currentFormState: INITIAL_STATE,
      disableStationAttributes: false,
    } as any;
    component = new GridFailureDetailsComponent(gridFailureSandbox, appState, actionsSubject);
    component.searchInput = { nativeElement: { value: 'x' } };

    gridFailureSandbox.gridFailureStations$ = of([new models.FailureStation()]);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set mapInteractionMode correctly', () => {
    component.gridFailureDetailsSandbox.oldVersion = false;
    component.failureLocationView = Globals.FAILURE_LOCATION_MAP;
    expect(component.mapInteractionMode).toBeTruthy();
  });

  it('should end subscriptions in Sandbox before the component going away', () => {
    let spy = spyOn(gridFailureSandbox, 'endSubscriptions');

    component['_subscription'] = { unsubscribe: () => {} } as any;
    component.ngOnDestroy();
    expect(spy).toHaveBeenCalled();
  });

  it('should registerEvents OnInit', () => {
    let spy = spyOn(gridFailureSandbox, 'registerEvents');

    component.ngOnInit();
    expect(spy).toHaveBeenCalled();
  });

  it('checks if resetCoords(value: string) works fine', () => {
    let spy1 = spyOn(gridFailureSandbox, 'resetCoords');

    component.resetCoords('1');
    expect(spy1).not.toHaveBeenCalled();

    component.resetCoords(null);
    expect(spy1).toHaveBeenCalled();
  });

  it('checks if setViewStateForReqProps works fine', () => {
    component.failureLocationView = Globals.FAILURE_LOCATION_NS;
    const spy = spyOn(gridFailureSandbox, 'setViewStateForReqProps');
    component.setViewStateForReqProps();
    expect(spy).toHaveBeenCalled();
  });

  it('should clear search input', () => {
    component.searchInput = { nativeElement: { value: 'x' } };
    component.clearSearchInput();
    expect(component.searchInput.nativeElement.value).toBe('');
  });

  it('should disable unnecessary required properties when setLocation was called', () => {
    component.gridFailureDetailsSandbox.gridFailureDetailsFormState$ = of(INITIAL_STATE, {
      ...component.gridFailureDetailsSandbox.currentFormState,
      value: {
        id: 1 as any,
        faultLocationArea: 'address' as any,

        branch: Globals.BUSINESS_RULE_FIELDS.branch.power,
      } as any,
    });
    component.setLocation();
    expect(component.failureLocationView).toBe('NS');
  });

  it('should reset selected station if no value exists', () => {
    const spy = spyOn(gridFailureSandbox, 'setSelectedStation');
    component.resetSelectedStation(null);
    expect(spy).toHaveBeenCalled();
  });

  it('should not reset selected station if value exists ', () => {
    const spy = spyOn(gridFailureSandbox, 'setSelectedStation');
    component.resetSelectedStation('x');
    expect(spy).not.toHaveBeenCalled();
  });

  it('should resize map via forceResize$.next(true)', () => {
    const spy = spyOn(component.mapOptions.forceResize$, 'next');
    component.resizeSetMap();
    expect(spy).toHaveBeenCalled();
  });

  it('should change to currentVersionMode in response to oldVersion=false', () => {
    const spy = spyOn(component.events$, 'next');
    component.changeMode();
    expect(spy).toHaveBeenCalledWith({ eventType: component['_modeEnum'].currentVersionMode });
  });

  it('should change to currentVersionMode in response to oldVersion=true', () => {
    const spy = spyOn(component.events$, 'next');
    component.gridFailureDetailsSandbox.oldVersion = true;
    component.changeMode();
    expect(spy).toHaveBeenCalledWith({ eventType: component['_modeEnum'].oldVersionMode });
  });

  it('should call deleteGridFailureStation function for delete event', async () => {
    const spy: any = spyOn(gridFailureSandbox, 'deleteGridFailureStation');
    await component.ngOnInit();
    component.gridOptions.context.eventSubject.next({ type: 'delete', data: new models.FailureStation() });
    expect(spy).toHaveBeenCalled();
  });

  it('should not call deleteGridFailureStation function for other (not delete) event', () => {
    const spy: any = spyOn(gridFailureSandbox, 'deleteGridFailureStation');
    component.ngOnInit();
    component.gridOptions.context.eventSubject.next({ type: 'test', data: new models.FailureStation() });
    expect(spy).not.toHaveBeenCalled();
  });

  it('should set member variable "currentAddress"', async () => {
    component.gridFailureDetailsSandbox.gridFailureDetailsFormState$ = of(INITIAL_STATE, {
      ...component.gridFailureDetailsSandbox.currentFormState,
      value: {
        id: 1 as any,
        postcode: 12345 as any,
        city: 'testCity' as any,
        district: 'testDistrict' as any,
        street: 'testStreet' as any,
        housenumber: 44 as any,
      } as any,
    });
    await component.ngOnInit();
    expect(component.currentAddress).toBe('12345 testCity (testDistrict), testStreet 44');
  });

  it('should set Branch State correctly', () => {
    const spy: any = spyOn(gridFailureSandbox, 'setBranchState');
    component.gridFailureDetailsSandbox.branches = [new models.FailureBranch()];
    component.setBranchValue('id1');
    expect(spy).toHaveBeenCalled();
  });

  it('should set gridOptions context icons', () => {
    component.setNewGridOptions('1');
    expect(component.gridOptions.context.icons).toBeDefined();
  });

  it('should return true for telecommunication', () => {
    let branch = Globals.BUSINESS_RULE_FIELDS.branch.telecommunication;
    expect(component.isLocationButtonForStationVisible(branch)).toBeTruthy();
  });

  it('should return true for power / ms', () => {
    let branch = Globals.BUSINESS_RULE_FIELDS.branch.power;
    let voltageLevel = Globals.FAILURE_LOCATION_MS;
    expect(component.isLocationButtonForStationVisible(branch, voltageLevel)).toBeTruthy();
  });

  it('should return false for other defined branches', () => {
    let branch = Globals.BUSINESS_RULE_FIELDS.branch.water;
    expect(component.isLocationButtonForStationVisible(branch)).toBeFalsy();
  });

  it('should return false for unknown branches', () => {
    let branch = 'X';
    expect(component.isLocationButtonForStationVisible(branch)).toBeUndefined();
  });

  it('should return true after isLocationRegionActiveForAddress() if failure location set to address', () => {
    component.failureLocationView = 'ABC';
    let expResult = component.isLocationRegionActiveForAddress();
    expect(expResult).toBeTruthy();
  });
  it('should return true after isLocationRegionActiveForStation() if failure location set to address', () => {
    component.failureLocationView = 'ABC';
    let expResult = component.isLocationRegionActiveForStation();
    expect(expResult).toBeTruthy();
  });
  it('should return true after isLocationRegionActiveForMap() if failure location set to address', () => {
    component.failureLocationView = 'ABC';
    let expResult = component.isLocationRegionActiveForMap();
    expect(expResult).toBeTruthy();
  });

  it('should call setViewStateForReqProps() after setLocationRegionToAddress() with different failureLocationView', () => {
    component.failureLocationView = 'ABC';
    const spy = spyOn(component, 'setViewStateForReqProps').and.callThrough();
    component.setLocationRegionToAddress();
    expect(spy).toHaveBeenCalled();
  });
  it('should not call setViewStateForReqProps() after setLocationRegionToAddress() with same failureLocationView', () => {
    component.failureLocationView = Globals.FAILURE_LOCATION_NS;
    const spy = spyOn(component, 'setViewStateForReqProps').and.callThrough();
    component.setLocationRegionToAddress();
    expect(spy).not.toHaveBeenCalled();
  });
  it('should call setViewStateForReqProps() after setLocationRegionToStation() with different failureLocationView', () => {
    component.failureLocationView = 'ABC';
    const spy = spyOn(component, 'setViewStateForReqProps').and.callThrough();
    component.setLocationRegionToStation();
    expect(spy).toHaveBeenCalled();
  });
  it('should not call setViewStateForReqProps() after setLocationRegionToStation() with same failureLocationView', () => {
    component.failureLocationView = Globals.FAILURE_LOCATION_MS;
    const spy = spyOn(component, 'setViewStateForReqProps').and.callThrough();
    component.setLocationRegionToStation();
    expect(spy).not.toHaveBeenCalled();
  });
  it('should call setViewStateForReqProps() after setLocationRegionToMap() with different failureLocationView', () => {
    component.failureLocationView = 'ABC';
    const spy = spyOn(component, 'setViewStateForReqProps').and.callThrough();
    component.setLocationRegionToMap();
    expect(spy).toHaveBeenCalled();
  });

  it('should not call setViewStateForReqProps() after setLocationRegionToMap() with same failureLocationView', () => {
    component.failureLocationView = Globals.FAILURE_LOCATION_MAP;
    const spy = spyOn(component, 'setViewStateForReqProps').and.callThrough();
    component.setLocationRegionToMap();
    expect(spy).not.toHaveBeenCalled();
  });

  it('should return true for detailFieldVisibility in case no config is provided', () => {
    component.mapOptions.visibilityConfiguration = null;
    expect(component.determineDetailFieldVisibility('description')).toBeTruthy();
  });

  it('should return true for detailFieldVisibility if config provides "show" string for a specified form field', () => {
    component.mapOptions.visibilityConfiguration = { fieldVisibility: { description: 'show' } as any } as any;
    expect(component.determineDetailFieldVisibility('description')).toBeTruthy();
  });

  it('should return false for detailFieldVisibility if config does not provides "show" string for a specified form field', () => {
    component.mapOptions.visibilityConfiguration = { fieldVisibility: { description: 'hide' } as any } as any;
    expect(component.determineDetailFieldVisibility('description')).toBeFalsy();
  });
});
