/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { GridFailureListComponent } from '@grid-failure-information-app/pages/grid-failure/grid-failure-list/grid-failure-list.component';
import { Router } from '@angular/router';
import { GridFailure } from '@grid-failure-information-app/shared/models';
import { GridFailureSandbox } from '@grid-failure-information-app/pages/grid-failure/grid-failure-list/grid-failure.sandbox';
import { Subscription, of, Subject } from 'rxjs';
import { UtilService } from '@grid-failure-information-app/shared/utility';
import { StateEnum } from '@grid-failure-information-app/shared/constants/enums';
import { MapOptions } from '@openk-libs/grid-failure-information-map/shared/models/map-options.model';

describe('GridFailureListComponent ', () => {
  let component: GridFailureListComponent;
  let router: Router;
  let sandbox: GridFailureSandbox;

  let subscription: Subscription;
  let utilService: UtilService;
  let appState$: any;
  let _gridApi: any;
  let filterInstance: any;
  let preConfig$: any;
  let mapOptions: MapOptions;

  beforeEach(() => {
    router = { navigate() {} } as any;
    sandbox = {
      filterOptions: { filterModel: {} },
      condensationList: [{ branch: 'F' }],
      endSubscriptions() {},
      clearGridFailureCondensation() {},
      condenseCondensationList() {},
      addItemToCondensationList() {},
      removeItemFromCondensationList() {},
      loadCondensedGridFailures() {},
      permissions: { reader: true },
      setFilteredGridFailureMapList() {},
      gridFailureListSuccess$: of({}),
    } as any;
    utilService = {
      displayNotification() {},
    } as any;
    mapOptions = new MapOptions();
    appState$ = { dispatch: () => {}, pipe: () => of(true), select: () => of() } as any;
    preConfig$ = of({ visibilityConfiguration: { fieldVisibility: {} } });
    subscription = { unsubscribe() {} } as any;
    _gridApi = { onFilterChanged() {}, setFilterModel(model: any) {} } as any;
    component = new GridFailureListComponent(sandbox, appState$, router, utilService);

    component['_gridApi'] = { setFilterModel: () => {} } as any;
    filterInstance = {
      getModel() {
        return { value: 'test' };
      },
    };
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call appropriate functions for edit event and ngOnInit', () => {
    mapOptions.visibilityConfiguration = { fieldVisibility: { failureClassification: 'show' } } as any;
    appState$ = { dispatch: () => {}, pipe: () => of(true), select: () => of(mapOptions as any) } as any;
    component = new GridFailureListComponent(sandbox, appState$, router, utilService);
    const spy: any = spyOn(router, 'navigate');
    component.ngOnInit();
    component.gridOptions.context.eventSubject.next({ type: 'edit', data: new GridFailure() });
    expect(spy).toHaveBeenCalled();
    expect(component.overviewColumnDefinition).toBeDefined();
    expect(component.condensationColumnDefinition).toBeDefined();
  });

  it('should not define column definitions without configuration', () => {
    mapOptions.visibilityConfiguration = null;
    appState$ = { dispatch: () => {}, pipe: () => of(true), select: () => of(mapOptions as any) } as any;
    component = new GridFailureListComponent(sandbox, appState$, router, utilService);

    component.ngOnInit();
    expect(component.overviewColumnDefinition).not.toBeDefined();
    expect(component.condensationColumnDefinition).not.toBeDefined();
  });

  it('should pass external filter if the appropriate conditions are met (published)', () => {
    let node: any = { data: { publicationStatus: StateEnum.PUBLISHED } };
    component.sandbox.publisherFilterIsActive = true;
    expect(component.doesExternalFilterPass(node)).toBeTruthy();
  });

  it('should pass external filter if the appropriate conditions are met (qualified)', () => {
    let node: any = { data: { statusIntern: StateEnum.QUALIFIED } };
    component.sandbox.publisherFilterIsActive = true;
    expect(component.doesExternalFilterPass(node)).toBeTruthy();
  });

  it('should should not pass external filter if the appropriate conditions are not met', () => {
    let node: any;
    expect(component.doesExternalFilterPass(node)).toBeFalsy();
  });

  it('should call appropriate functions for add event', () => {
    sandbox.condensationList = [];
    const spy1: any = spyOn(sandbox, 'addItemToCondensationList');
    const spy2: any = spyOn((component as any)._utilService, 'displayNotification');
    component.ngOnInit();
    component.gridOptions.context.eventSubject.next({ type: 'add', data: new GridFailure() });
    expect(spy1).toHaveBeenCalled();
    component.ngOnInit();
    let gridFailure: any = { branch: 'F' };
    sandbox.condensationList = [gridFailure];
    component.gridOptions.context.eventSubject.next({ type: 'add', data: gridFailure });
    expect(spy1).toHaveBeenCalled();
    component.ngOnInit();
    gridFailure = { branch: 'S' };
    component.gridOptions.context.eventSubject.next({ type: 'add', data: gridFailure });
    expect(spy2).toHaveBeenCalled();
  });

  it('should call appropriate functions for addAllItems event', () => {
    const spy: any = spyOn(component, 'addCompleteTable');
    component.ngOnInit();
    component.gridOptions.context.eventSubject.next({ type: 'addAllItems', data: new GridFailure() });
    expect(spy).toHaveBeenCalled();
  });

  it('should call appropriate functions for initialLoad event', () => {
    const spy: any = spyOn(component as any, '_changeMode');
    component.ngOnInit();
    component.gridOptions.context.eventSubject.next({ type: 'initialLoad' });
    expect(spy).toHaveBeenCalled();
  });

  it('should call appropriate functions for remove event', () => {
    const spy: any = spyOn(sandbox, 'removeItemFromCondensationList');
    component.ngOnInit();
    component.gridOptionsCondensation.context.eventSubject.next({ type: 'remove', data: new GridFailure() });
    expect(spy).toHaveBeenCalled();
  });

  it('should unsubscribe OnDestroy', () => {
    const spy: any = spyOn(sandbox, 'endSubscriptions');
    component['_subscription'] = new Subscription();
    component.ngOnDestroy();
    expect(spy).toHaveBeenCalled();
  });

  it('should call and check if condenseChoosedGridFailureInformations() works fine', () => {
    const spy1: any = spyOn(component, 'clearGridFailureCondensation');
    const spy2: any = spyOn(sandbox, 'condenseCondensationList');
    const spy3: any = spyOn((component as any)._utilService, 'displayNotification');

    component.condenseChoosedGridFailureInformations();

    expect(spy1).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();

    sandbox.condensationList = [];
    component.condenseChoosedGridFailureInformations();

    expect(spy3).toHaveBeenCalled();
  });

  it('should call and check if onGridReady(params) works fine', () => {
    const mockGridApi = {
      setFilterModel() {},
    } as any;
    const spy2: any = spyOn(component, 'initGridFilter');
    component['_setInitialGridOptions']();
    component.gridOptions.onGridReady({ api: mockGridApi } as any);
    expect(spy2).toHaveBeenCalled();
  });

  it('should call and check if changeToSelectionMode() works fine', () => {
    const spy: any = spyOn(component.events$, 'next');
    component.showCondensationTable = false;

    component.changeToSelectionMode();

    expect(component.showCondensationTable).toBeTruthy();
    expect(spy).toHaveBeenCalledWith({ eventType: 'overviewTableSelectionMode' });
  });

  it('should call and check if loadCondensedGridFailures(id: string) works fine', () => {
    component.showCondensationTable = false;
    component.loadCondensedGridFailures('');

    expect(component.showCondensationTable).toBeTruthy();
  });

  it('should call appropriate functions for loadCondensedItems event', () => {
    const spy: any = spyOn(component, 'loadCondensedGridFailures');
    component.ngOnInit();
    component.gridOptions.context.eventSubject.next({ type: 'loadCondensedItems', data: { id: '' } });
    expect(spy).toHaveBeenCalled();
  });

  it('should call appropriate functions for edit event', () => {
    const spy: any = spyOn(router, 'navigate');
    component.ngOnInit();
    component.gridOptionsCondensation.context.eventSubject.next({ type: 'edit', data: new GridFailure() });
    expect(spy).toHaveBeenCalled();
  });

  it('should call appropriate functions for initialLoad event', () => {
    const spy: any = spyOn(component as any, '_changeMode');
    component.ngOnInit();
    component.gridOptionsCondensation.context.eventSubject.next({ type: 'initialLoad' });
    expect(spy).toHaveBeenCalled();
  });

  it('should navigate to detail when navigateToDetail-function was called', () => {
    const spy: any = spyOn(router, 'navigate');
    component.navigateToDetails('test');
    expect(spy).toHaveBeenCalledWith(['/grid-failures', 'test']);
  });
  it('should call setMapFilter and set filterOptions and MapList', () => {
    component.sandbox.filteredGridFailureMapList = [];
    const mockGridApi = {
      rowModel: { rowsToDisplay: [{ data: new GridFailure() }] },
      forEachNodeAfterFilter: () => {},
      setFilterModel() {},
      getFilterModel() {},
    } as any;
    const spy2: any = spyOn(mockGridApi, 'getFilterModel').and.returnValue('test');
    component.initGridFilter({ api: mockGridApi });
    component.setMapFilter();
    expect(component.sandbox.filterOptions['filterModel'][0][0]).toEqual('t');
  });
});
