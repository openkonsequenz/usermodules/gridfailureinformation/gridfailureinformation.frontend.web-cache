/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { Store, ActionsSubject } from '@ngrx/store';
import { SetValueAction, ResetAction, MarkAsTouchedAction } from 'ngrx-forms';
import * as store from '@grid-failure-information-app/shared/store';
import { Observable, combineLatest } from 'rxjs';
import * as gridFailureActions from '@grid-failure-information-app/shared/store/actions/grid-failures.action';
import { Router } from '@angular/router';
import { GridFailure } from '@grid-failure-information-app/shared/models/grid-failure.model';
import { FailureStation } from '@grid-failure-information-app/shared/models';
import { BaseSandbox } from '@grid-failure-information-app/shared/sandbox/base.sandbox';
import * as fromGridFailuresDetailFormReducer from '@grid-failure-information-app/shared/store/reducers/grid-failures/grid-failure-details-form.reducer';
import { ofType } from '@ngrx/effects';
import { map, skipWhile, take } from 'rxjs/operators';
import { StateEnum, RolesEnum } from '@grid-failure-information-app/shared/constants/enums';
import { Subscription } from 'rxjs/Subscription';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { Globals } from '@grid-failure-information-app/shared/constants/globals';

@Injectable()
export class GridFailureSandbox extends BaseSandbox {
  public gridFailureList$: Observable<GridFailure[]> = this.appState$.select(store.getGridFailuresData);
  public gridFailureListSuccess$: Observable<GridFailure[]> = this.actionsSubject.pipe(
    ofType(gridFailureActions.loadGridFailuresSuccess.type),
    map(action => action['payload'])
  );
  public gridFailureListLoading$: Observable<boolean> = this.appState$.select(store.getGridFailuresLoading);
  public failureStations$: Observable<FailureStation[]> = this.appState$.select(store.getStationsData);

  public overviewGridFailureList: GridFailure[] = new Array();
  public filteredGridFailureMapList: GridFailure[];
  public filterOptions: any = {};
  public publisherFilterIsActive: boolean = false;
  public qualifierFilterIsActive: boolean = false;
  public condensationList: GridFailure[] = new Array();
  public condenseId: string = null;
  public condenseBranch: string = null;
  public condenseStatusIntern: string = null;
  public StateEnum = StateEnum;
  public RolesEnum = RolesEnum;
  public isReminderActive$: Observable<boolean> = this.appState$.select(store.getGridFailureReminderData);

  private _timer;

  constructor(protected appState$: Store<store.State>, private _router: Router, public actionsSubject: ActionsSubject) {
    super(appState$);
    this._registerEvents();
    this._startPolling();
  }

  public loadGridFailures(): void {
    this.appState$.dispatch(gridFailureActions.loadGridFailures());
  }

  public createGridFailure() {
    this.appState$.dispatch(new SetValueAction(fromGridFailuresDetailFormReducer.FORM_ID, fromGridFailuresDetailFormReducer.INITIAL_STATE.value));
    this.appState$.dispatch(new ResetAction(fromGridFailuresDetailFormReducer.FORM_ID));
    this.appState$.dispatch(new MarkAsTouchedAction(fromGridFailuresDetailFormReducer.FORM_ID));
    this._router.navigateByUrl('/grid-failures/new');
  }

  public clearGridFailureCondensation(): void {
    this.condenseId = null;
    this.condenseBranch = null;
    this.condensationList = [];
  }
  public cancelCondesation(): void {
    this.condensationList = [];
    this.gridFailureList$.pipe(take(1)).subscribe(gridFailures => (this.overviewGridFailureList = gridFailures));
  }
  public addItemToCondensationList(data: GridFailure): void {
    if (this.condensationList.find(item => item.id == data.id) != null || data.condensed === true) {
      return;
    } else {
      this.condensationList = [...this.condensationList, data];
      this.overviewGridFailureList = this.overviewGridFailureList.filter(item => item.id !== data.id);
    }
  }

  public removeItemFromCondensationList(data: GridFailure): void {
    this.condensationList = this.condensationList.filter(item => item.id !== data.id);
    this.overviewGridFailureList = [...this.overviewGridFailureList, data];
  }

  public condenseCondensationList(): void {
    const list: string[] = new Array();
    this.condensationList.forEach(item => {
      list.push(item.id);
    });
    if (this.condenseId) {
      this.appState$.dispatch(gridFailureActions.putGridFailuresCondensation({ gridFailureId: this.condenseId, payload: list }));
    } else {
      this.appState$.dispatch(gridFailureActions.postGridFailuresCondensation({ payload: list }));
    }
  }

  public loadCondensedGridFailures(id: string): void {
    this.condenseId = id;
    this.appState$.dispatch(gridFailureActions.loadCondensedGridFailures({ payload: id }));
  }

  private _registerEvents(): void {
    this._initCondesationList();
    this._initOverviewList();
  }

  private _initCondesationList() {
    const allStations$ = this._loadAllStations();
    combineLatest(
      allStations$,
      this.actionsSubject.pipe(
        ofType(gridFailureActions.loadCondensedGridFailuresSuccess),
        map((action: { payload: GridFailure[] }) => action.payload)
      )
    ).subscribe(([stations, condensedGridFailures]) => {
      if (condensedGridFailures && condensedGridFailures.length) {
        this.condensationList = this._getGridFailureWithReadableStationInformation(condensedGridFailures, stations);
        const condensedGridFailureFromOverview = this.overviewGridFailureList.find(gridFailure => gridFailure.id === this.condenseId);
        if (!!condensedGridFailureFromOverview) {
          this.condenseStatusIntern = condensedGridFailureFromOverview.statusIntern;
        }
        this.condenseBranch = condensedGridFailures[0].branch;
      }
    });
  }

  private _initOverviewList() {
    const allStations$ = this._loadAllStations();
    combineLatest(allStations$, this.gridFailureList$).subscribe(([stations, gridFailures]) => {
      if (gridFailures && gridFailures.length) {
        this.overviewGridFailureList = this._getGridFailureWithReadableStationInformation(gridFailures, stations);
      }
    });
  }

  private _getGridFailureWithReadableStationInformation(condensedGridFailures: GridFailure[], stations: FailureStation[]): GridFailure[] {
    return condensedGridFailures.map(cgf => {
      cgf.stationDescription = 'value' in cgf.stationIds ? this._getStationDescription((cgf.stationIds as any).value, stations) : '';
      return cgf;
    }) as any;
  }
  public setFilteredGridFailureMapList(gridFailures: GridFailure[]) {
    this.filteredGridFailureMapList = gridFailures;
  }
  private _loadAllStations(): Observable<FailureStation[]> {
    return this.actionsSubject.pipe(
      ofType(gridFailureActions.loadStationsSuccess.type),
      map(action => action['payload']),
      take(1)
    );
  }

  private _getStationDescription(stationIds: string[], stations: FailureStation[]): string {
    let ret: string = '';
    if (!!stationIds && stationIds.length > 0) {
      stationIds.forEach(item => {
        const failureStation: FailureStation = stations.find(s => s.id === item) as any;
        if (!!failureStation && !!failureStation.stationName) {
          ret += `${failureStation.stationName} (${failureStation.stationId}), `;
        }
      });
      ret = ret.substr(0, ret.lastIndexOf(', '));
    }
    return ret;
  }

  private _startPolling() {
    this._timer = TimerObservable.create(Globals.REMINDER_JOB_POLLING_START_DELAY, Globals.REMINDER_JOB_POLLING_INTERVALL);
    this._timer.subscribe(() => this.appState$.dispatch(gridFailureActions.loadFailureReminder()));
  }
}
