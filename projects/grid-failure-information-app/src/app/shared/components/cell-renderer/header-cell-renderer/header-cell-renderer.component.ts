/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component } from '@angular/core';
import { IHeaderAngularComp } from 'ag-grid-angular';
import { ModeEnum } from '@grid-failure-information-app/shared/constants/enums';

@Component({
  selector: 'app-header-cell-renderer',
  templateUrl: './header-cell-renderer.component.html',
  styleUrls: ['./header-cell-renderer.component.scss'],
})
export class HeaderCellRendererComponent implements IHeaderAngularComp {
  public addHeaderIcon = null;

  private _params: any;
  private _modeEnum = ModeEnum;

  agInit(params): void {
    this._params = params;
    this._updateIcon();
    if (params.context && params.context.icons) {
      const contextIcons = params.context.icons;
      this.addHeaderIcon = !!contextIcons && !!contextIcons.add;
    }
  }

  public clicked(eventType: string): void {
    this._params.context.eventSubject.next({ type: eventType, data: this._params.data });
  }

  private _updateIcon(): void {
    this._params.context.eventSubject.subscribe((event: any) => {
      switch (event.eventType) {
        case this._modeEnum.InitialMode:
          this.addHeaderIcon = false;
          break;
        case this._modeEnum.OverviewTableSelectionMode:
          this.addHeaderIcon = true;
          break;
        default:
          break;
      }
    });
  }
}
