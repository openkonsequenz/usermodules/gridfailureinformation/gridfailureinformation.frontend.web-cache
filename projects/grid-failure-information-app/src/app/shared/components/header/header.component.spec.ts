/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { HeaderComponent } from '@grid-failure-information-app/shared/components/header/header.component';
import { GridFailureSandbox } from '@grid-failure-information-app/pages/grid-failure/grid-failure-list/grid-failure.sandbox';
import { of } from 'rxjs';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let router: any;
  let sandbox: GridFailureSandbox;

  beforeEach(() => {
    router = { navigateByUrl() {} } as any;
    component = new HeaderComponent(router, sandbox);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate to overview after call navigateHome', () => {
    const spy = spyOn(router, 'navigateByUrl').and.returnValue({ then: () => of(true) });
    component.navigateHome();
    expect(spy).toHaveBeenCalledWith('/grid-failures');
  });

  it('should navigate to logout after call navigateToLogout', () => {
    const spy = spyOn(router, 'navigateByUrl').and.returnValue({ then: () => of(true) });
    component.navigateToLogout();
    expect(spy).toHaveBeenCalledWith('/logout');
  });
});
