/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { LayoutSandbox } from '@grid-failure-information-app/shared/containers/layout/layout.sandbox';
import * as store from '@grid-failure-information-app/shared/store';
import { Store } from '@ngrx/store';
import { of, Subscription } from 'rxjs';
import { LayoutContainerComponent } from './layout.container';

describe('LayoutContainer', () => {
  let component: LayoutContainerComponent = {} as any;
  let sandbox: LayoutSandbox;
  let _configService: any;
  let appState$: Store<store.State>;

  beforeEach(() => {
    appState$ = { dispatch: () => {}, pipe: () => of(true), select: () => of(true) } as any;
    _configService = { get: () => ({ userImageFolder: 'path' }) } as any;

    component = new LayoutContainerComponent(_configService, appState$, sandbox);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call ngOnInit', () => {
    const spy1 = spyOn(component as any, 'registerEvents');
    component.ngOnInit();
    expect(spy1).toHaveBeenCalled();
  });
  it('should call ngOnDestroy', () => {
    (component as any).subscriptions = [new Subscription()];
    const spy1 = spyOn((component as any).subscriptions[0], 'unsubscribe');
    component.ngOnDestroy();
    expect(spy1).toHaveBeenCalled();
  });
});
