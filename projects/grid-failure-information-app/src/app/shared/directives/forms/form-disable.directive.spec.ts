/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { FormDisableDirective } from '@grid-failure-information-app/shared/directives/forms/form-disable.directive';
import { async } from '@angular/core/testing';
import { of } from 'rxjs/observable/of';
import { FormState } from 'ngrx-forms';
import { PermissionsModel } from '@grid-failure-information-app/shared/models/permissions.model';
import { RolesEnum, StateEnum } from '@grid-failure-information-app/shared/constants/enums';

describe('FormDisableDirective', () => {
  let directive: FormDisableDirective;
  let viewContainerRef: any;
  let appState: any;

  beforeEach(async(() => {
    viewContainerRef = {
      createEmbeddedView: () => {},
      clear: () => {},
      element: { nativeElement: { elements: [{ classList: {}, disabled: undefined, childNodes: [] }] } },
    };

    appState = {
      pipe: () => of(),
      dispatch: () => {},
      select: () => of({ roles: ['grid-failure-reader'] }),
      map: () => of({ reader: true }),
    };
    directive = new FormDisableDirective(appState as any);
  }));

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });

  it('should not dispatch action if formState is disabled', () => {
    let state: FormState<any> = { isDisabled: true } as any;
    let dispatchSpy = spyOn(directive['_appState$'], 'dispatch');
    directive.ngrxFormState = state;
    expect(dispatchSpy).toHaveBeenCalledTimes(0);
  });

  it('should dispatch action if formState is not disabled', () => {
    let permission: PermissionsModel = new PermissionsModel([RolesEnum.READER]);
    directive['_permissions$'] = of(permission);
    let state: FormState<any> = { isDisabled: false } as any;
    let dispatchSpy = spyOn(directive['_appState$'], 'dispatch');
    directive.ngrxFormState = state;
    expect(dispatchSpy).toHaveBeenCalled();
  });

  it('should disable from if user is publisher and status is qualified', () => {
    let permission: PermissionsModel = new PermissionsModel([RolesEnum.PUBLISHER]);
    directive['_permissions$'] = of(permission);
    let state: FormState<any> = { isDisabled: false, value: { statusIntern: StateEnum.QUALIFIED } } as any;
    let dispatchSpy = spyOn(directive['_appState$'], 'dispatch');
    directive.ngrxFormState = state;
    expect(dispatchSpy).toHaveBeenCalled();
  });

  it('should disable from if status is completed', () => {
    let permission: PermissionsModel = new PermissionsModel([RolesEnum.ADMIN]);
    directive['_permissions$'] = of(permission);
    let state: FormState<any> = { isDisabled: false, value: { statusIntern: StateEnum.COMPLETED } } as any;
    let dispatchSpy = spyOn(directive['_appState$'], 'dispatch');
    directive.ngrxFormState = state;
    expect(dispatchSpy).toHaveBeenCalled();
  });

  it('should disable from if status is canceled', () => {
    let permission: PermissionsModel = new PermissionsModel([RolesEnum.ADMIN]);
    directive['_permissions$'] = of(permission);
    let state: FormState<any> = { isDisabled: false, value: { statusIntern: StateEnum.CANCELED } } as any;
    let dispatchSpy = spyOn(directive['_appState$'], 'dispatch');
    directive.ngrxFormState = state;
    expect(dispatchSpy).toHaveBeenCalled();
  });
});
