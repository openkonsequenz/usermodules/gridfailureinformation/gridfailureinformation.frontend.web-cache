/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { of } from 'rxjs/observable/of';
import { State } from '@grid-failure-information-app/shared/store';
import { Store } from '@ngrx/store';
import { EnableAction, ResetAction, NgrxFormDirective } from 'ngrx-forms';
import { VisibleByDependentFieldDirective } from './visible-by-dependent-fields.directive';

describe('VisibleByDependentFieldDirective ', () => {
  let directive: VisibleByDependentFieldDirective;
  let appState: Store<State>;
  let form: NgrxFormDirective<any>;

  beforeEach(() => {
    appState = of({
      pipe: () => {},
      dispatch: () => {},
      select: () => {},
      map: () => {},
    }) as any;
    form = { state: { isEnabled: true } } as any;
    directive = new VisibleByDependentFieldDirective(appState as any, form);
  });

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });

  it('should set isVisible property', () => {
    directive = new VisibleByDependentFieldDirective({ dispatch: () => null } as any, form);

    directive.ngrxFormControl = { state: { id: 'x' } } as any;
    directive.visibleByDependentField = true;
    expect(directive['_isVisible']).toBeTruthy();
  });

  it('should dispatch EnableAction if isVisible = true', () => {
    directive = new VisibleByDependentFieldDirective({ dispatch: () => null } as any, form);
    let dispatchSpy = spyOn(directive['_appState$'], 'dispatch');

    directive.ngrxFormControl = { state: { id: 'x' } } as any;
    directive.visibleByDependentField = true;
    expect(dispatchSpy).toHaveBeenCalledWith(new EnableAction(directive.ngrxFormControl.state.id));
  });

  it('should dispatch ResetAction if isVisible = false', () => {
    directive = new VisibleByDependentFieldDirective({ dispatch: () => null } as any, form);
    let dispatchSpy = spyOn(directive['_appState$'], 'dispatch');

    directive.ngrxFormControl = { state: { id: 'x' } } as any;
    directive.visibleByDependentField = false;
    expect(dispatchSpy).toHaveBeenCalledWith(new ResetAction(directive.ngrxFormControl.state.id));
  });
});
