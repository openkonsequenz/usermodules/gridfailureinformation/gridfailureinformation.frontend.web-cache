/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Directive, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { PermissionsModel } from '@grid-failure-information-app/shared/models/permissions.model';
import { User } from '@grid-failure-information-app/shared/models/user';
import * as store from '@grid-failure-information-app/shared/store';
import { Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

/**
 * This directive shows/hides html elements depending on user rights
 *
 * @author Martin Gardyan <martin.gardyan@pta.de>
 * @export
 * @class VisibleByRightDirective
 * @implements {OnInit}
 * @implements {OnDestroy}
 */
@Directive({
  selector: '[visibleByRight]',
})
export class VisibleByRightDirective implements OnInit, OnDestroy {
  @Input() visibleByRight?: string[];

  private _endSubscriptions$: Subject<boolean> = new Subject();
  private _permissions$: Observable<PermissionsModel> = this._appState$
    .select(store.getUser)
    .pipe(takeUntil(this._endSubscriptions$))
    .map((user: User) => new PermissionsModel(user.roles));

  constructor(private _templateRef: TemplateRef<any>, private _viewContainer: ViewContainerRef, private _appState$: Store<store.State>) {}

  ngOnInit() {
    this._permissions$.subscribe(permissions => {
      if (
        permissions.admin ||
        this.visibleByRight.includes(permissions.qualifier) ||
        this.visibleByRight.includes(permissions.publisher) ||
        this.visibleByRight.includes(permissions.creator) ||
        this.visibleByRight.includes(permissions.reader)
      ) {
        this._viewContainer.createEmbeddedView(this._templateRef);
      } else {
        this._viewContainer.clear();
      }
    });
  }

  ngOnDestroy() {
    this._endSubscriptions$.next(true);
  }
}
