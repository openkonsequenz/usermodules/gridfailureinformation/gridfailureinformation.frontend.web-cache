/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
export class Globals {
  public static DATE_TIME_PLACEHOLDER_PATTERN: string = 'tt.mm.jjjj / HH:MM';
  public static HELP_URL: string = './assets/userDocumentation/userDocumentation/userDocumentation.adoc.html';
  public static CONDENSATION_TABLE_PAGINATION_PAGE_SIZE: number = 4;
  public static DATE_FORMAT: string = 'dd.MM.yyyy';
  public static DATE_TIME_FORMAT: string = 'dd.MM.yyyy / HH:mm';
  public static STATUS_INTERN_FIELD: string = 'statusIntern';
  public static STATUS_PUBLICATION_FIELD: string = 'publicationStatus';
  public static SET_FILTER_COMPONENT: string = 'setFilterComponent';
  public static FAILURE_LOCATION_NS: string = 'NS';
  public static FAILURE_LOCATION_HS: string = 'HS';
  public static FAILURE_LOCATION_MS: string = 'MS';

  public static FAILURE_LOCATION_MAP: string = 'map';
  public static REMINDER_JOB_POLLING_INTERVALL = 60000; //60 seconds
  public static REMINDER_JOB_POLLING_START_DELAY = 2000; // 2 seconds

  public static LOCALE_TEXT: any = {
    equals: 'ist gleich',
    notEqual: 'ist ungleich',
    greaterThan: 'ist größer als',
    lessThan: 'ist kleiner als',
    inRange: 'ist zwischen',
    andCondition: 'und',
    orCondition: 'oder',
    resetFilter: 'Filter löschen',
    applyFilter: 'Filter anwenden',
    contains: 'enthält',
    notContains: 'enthält nicht',
    startsWith: 'beginnt mit',
    endsWith: 'endet mit',
  };
  public static PROPERTIES_TO_BOX: string[] = ['addressPolygonPoints', '__formBranch', 'distributionGroupUuids', 'stationIds', 'publicationChannels'];

  public static BUSINESS_RULE_FIELDS: any = {
    branch: {
      power: 'S',
      gas: 'G',
      telecommunication: 'TK',
      districtHeating: 'F',
      water: 'W',
      secondaryTechnology: 'ST',
      noBranch: 'OS',
    },
  };
}
