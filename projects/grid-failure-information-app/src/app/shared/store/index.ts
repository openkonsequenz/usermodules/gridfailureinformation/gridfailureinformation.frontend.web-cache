/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/**
 * Every reducer module's default export is the reducer function itself. In
 * addition, each module should export a type or interface that describes
 * the state of the reducer plus any selector functions. The `* as`
 * notation packages up all of the exports into a single object.
 */
import * as fromSettings from '@grid-failure-information-app/shared/store/reducers/settings.reducer';
import * as fromGridFailures from '@grid-failure-information-app/shared/store/reducers/grid-failures/grid-failures.reducer';
import * as fromGridFailureVersions from '@grid-failure-information-app/shared/store/reducers/grid-failures/grid-failure-versions.reducer';
import * as fromGridFailureBranches from '@grid-failure-information-app/shared/store/reducers/grid-failures/grid-failure-branches.reducer';
import * as fromGridFailureClassifications from '@grid-failure-information-app/shared/store/reducers/grid-failures/grid-failure-classifications.reducer';
import * as fromGridFailureStates from '@grid-failure-information-app/shared/store/reducers/grid-failures/grid-failure-states.reducer';
import * as fromGridFailureRadii from '@grid-failure-information-app/shared/store/reducers/grid-failures/grid-failure-radii.reducer';
import * as fromGridFailureExpectedReasons from '@grid-failure-information-app/shared/store/reducers/grid-failures/grid-failure-expected-reasons.reducer';
import * as fromStations from '@grid-failure-information-app/shared/store/reducers/grid-failures/stations.reducer';
import * as fromGridFailurePolygon from '@grid-failure-information-app/shared/store/reducers/grid-failures/grid-failure-polygon.reducer';
import * as fromGridFailureStations from '@grid-failure-information-app/shared/store/reducers/grid-failures/grid-failure-stations.reducer';
import * as fromHistGridFailureStations from '@grid-failure-information-app/shared/store/reducers/grid-failures/hist-grid-failure-stations.reducer';
import * as fromGridFailuresDetailForm from '@grid-failure-information-app/shared/store/reducers/grid-failures/grid-failure-details-form.reducer';
import * as fromCondensedGridFailures from '@grid-failure-information-app/shared/store/reducers/grid-failures/condensed-grid-failures.reducer';
import * as fromDistributionGroups from '@grid-failure-information-app/shared/store/reducers/distribution-groups/distribution-groups.reducer';
import * as fromGridFailureReminder from '@grid-failure-information-app/shared/store/reducers/grid-failures/grid-failure-reminder.reducer';
import * as fromDistributionGroupsDetailForm from '@grid-failure-information-app/shared/store/reducers/distribution-groups/distribution-group-details-form.reducer';
import * as fromDistributionGroupMembers from '@grid-failure-information-app/shared/store/reducers/distribution-groups/distribution-group-members.reducer';
import * as fromGridFailureDistributionGroups from '@grid-failure-information-app/shared/store/reducers/grid-failures/grid-failure-distribution-groups.reducer';
import * as fromGridFailurePublicationChannels from '@grid-failure-information-app/shared/store/reducers/grid-failures/grid-failure-publication-channels.reducer';

import { createFeatureSelector } from '@ngrx/store';
import { createSelector } from 'reselect';
import { GridFailure, DistributionGroup } from '@grid-failure-information-app/shared/models';
import { FormGroupState } from 'ngrx-forms';

/**
 * We treat each reducer like a table in a database. This means
 * our top level state interface is just a map of keys to inner state types.
 */
export interface State {
  settings: fromSettings.State;
  initialEmailContent: fromSettings.State['initialEmailContent'];
}

export interface GridFailureState {
  gridFailures: fromGridFailures.State;
  gridFailuresDetailForm: FormGroupState<GridFailure>;
  gridFailureVersions: fromGridFailureVersions.State;
  gridFailureBranches: fromGridFailureBranches.State;
  gridFailureClassifications: fromGridFailureClassifications.State;
  gridFailureStates: fromGridFailureStates.State;
  gridFailureRadii: fromGridFailureRadii.State;
  gridFailureExpectedReasons: fromGridFailureExpectedReasons.State;
  gridFailureStations: fromGridFailureStations.State;
  histGridFailureStations: fromHistGridFailureStations.State;
  gridFailurePolygon: fromGridFailurePolygon.State;
  stations: fromStations.State;
  condensedGridFailures: fromCondensedGridFailures.State;
  gridFailureDistributionGroups: fromGridFailureDistributionGroups.State;
  gridFailurePublicationChannels: fromGridFailurePublicationChannels.State;
  gridFailureReminder: fromGridFailureReminder.State;
}

export interface DistributionGroupState {
  distributionGroups: fromDistributionGroups.State;
  distributionGroupDetailForm: FormGroupState<DistributionGroup>;
  distributionGroupMembers: fromDistributionGroupMembers.State;
}

/**
 * Because metareducers take a reducer function and return a new reducer,
 * we can use our compose helper to chain them together. Here we are
 * using combineReducers to make our top level reducer, and then
 * wrapping that in storeLogger. Remember that compose applies
 * the result from right to left.
 */
export const reducers = {
  settings: fromSettings.reducer,
};

export const gridFailureReducers = {
  gridFailures: fromGridFailures.reducer,
  gridFailuresDetailForm: fromGridFailuresDetailForm.reducer,
  gridFailureVersions: fromGridFailureVersions.reducer,
  gridFailureBranches: fromGridFailureBranches.reducer,
  gridFailureClassifications: fromGridFailureClassifications.reducer,
  gridFailureStates: fromGridFailureStates.reducer,
  gridFailureRadii: fromGridFailureRadii.reducer,
  gridFailureExpectedReasons: fromGridFailureExpectedReasons.reducer,
  gridFailureStations: fromGridFailureStations.reducer,
  histGridFailureStations: fromHistGridFailureStations.reducer,
  gridFailurePolygon: fromGridFailurePolygon.reducer,
  stations: fromStations.reducer,
  condensedGridFailures: fromCondensedGridFailures.reducer,
  gridFailureDistributionGroups: fromGridFailureDistributionGroups.reducer,
  gridFailurePublicationChannels: fromGridFailurePublicationChannels.reducer,
  gridFailureReminder: fromGridFailureReminder.reducer,
};

export const distributionGroupReducers = {
  distributionGroups: fromDistributionGroups.reducer,
  distributionGroupDetailForm: fromDistributionGroupsDetailForm.reducer,
  distributionGroupMembers: fromDistributionGroupMembers.reducer,
};

/**
 * Every reducer module exports selector functions, however child reducers
 * have no knowledge of the overall state tree. To make them useable, we
 * need to make new selectors that wrap them.
 */

/**
 * Settings store functions
 */
export const getSettingsState = (state: State) => state.settings;

export const getSelectedLanguage = createSelector(getSettingsState, fromSettings.getSelectedLanguage);
export const getSelectedCulture = createSelector(getSettingsState, fromSettings.getSelectedCulture);
export const getAvailableLanguages = createSelector(getSettingsState, fromSettings.getAvailableLanguages);
export const getUser = createSelector(getSettingsState, fromSettings.getUser);
export const getPreConfiguration = createSelector(getSettingsState, fromSettings.getPreConfiguration);
export const getInitialEmailContent = createSelector(getSettingsState
, fromSettings.getInitialEmailContent);
export const getExportChannels = createSelector(getSettingsState, fromSettings.getExportChannels);

/**
 * GridFailures store functions
 */
export const selectGridFailuresState = createFeatureSelector<GridFailureState>('grid-failure');
// GridFailures list
export const selectGridFailures = createSelector(selectGridFailuresState, (state: GridFailureState) => state.gridFailures);
export const getGridFailuresLoaded = createSelector(selectGridFailures, fromGridFailures.getLoaded);
export const getGridFailuresLoading = createSelector(selectGridFailures, fromGridFailures.getLoading);
export const getGridFailuresFailed = createSelector(selectGridFailures, fromGridFailures.getFailed);
export const getGridFailuresData = createSelector(selectGridFailures, fromGridFailures.getData);

// GridFailures details
export const selectGridFailuresDetails = createSelector(selectGridFailuresState, (state: GridFailureState) => state.gridFailuresDetailForm);
export const getGridFailuresDetails = createSelector(selectGridFailuresDetails, fromGridFailuresDetailForm.getFormState);

// GridFailure versions
export const selectGridFailureVersions = createSelector(selectGridFailuresState, (state: GridFailureState) => state.gridFailureVersions);
export const getGridFailureVersionsLoaded = createSelector(selectGridFailureVersions, fromGridFailureVersions.getLoaded);
export const getGridFailureVersionsLoading = createSelector(selectGridFailureVersions, fromGridFailureVersions.getLoading);
export const getGridFailureVersionsFailed = createSelector(selectGridFailureVersions, fromGridFailureVersions.getFailed);
export const getGridFailureVersionsData = createSelector(selectGridFailureVersions, fromGridFailureVersions.getData);

// GridFailure branches
export const selectGridFailureBranches = createSelector(selectGridFailuresState, (state: GridFailureState) => state.gridFailureBranches);
export const getGridFailureBranchesLoaded = createSelector(selectGridFailureBranches, fromGridFailureBranches.getLoaded);
export const getGridFailureBranchesLoading = createSelector(selectGridFailureBranches, fromGridFailureBranches.getLoading);
export const getGridFailureBranchesFailed = createSelector(selectGridFailureBranches, fromGridFailureBranches.getFailed);
export const getGridFailureBranchesData = createSelector(selectGridFailureBranches, fromGridFailureBranches.getData);

// GridFailure classifications
export const selectGridFailureClassifications = createSelector(selectGridFailuresState, (state: GridFailureState) => state.gridFailureClassifications);
export const getGridFailureClassificationsLoaded = createSelector(selectGridFailureClassifications, fromGridFailureClassifications.getLoaded);
export const getGridFailureClassificationsLoading = createSelector(selectGridFailureClassifications, fromGridFailureClassifications.getLoading);
export const getGridFailureClassificationsFailed = createSelector(selectGridFailureClassifications, fromGridFailureClassifications.getFailed);
export const getGridFailureClassificationsData = createSelector(selectGridFailureClassifications, fromGridFailureClassifications.getData);

// GridFailure states
export const selectGridFailureStates = createSelector(selectGridFailuresState, (state: GridFailureState) => state.gridFailureStates);
export const getGridFailureStatesLoaded = createSelector(selectGridFailureStates, fromGridFailureStates.getLoaded);
export const getGridFailureStatesLoading = createSelector(selectGridFailureStates, fromGridFailureStates.getLoading);
export const getGridFailureStatesFailed = createSelector(selectGridFailureStates, fromGridFailureStates.getFailed);
export const getGridFailureStatesData = createSelector(selectGridFailureStates, fromGridFailureStates.getData);

// GridFailure radii
export const selectGridFailureRadii = createSelector(selectGridFailuresState, (state: GridFailureState) => state.gridFailureRadii);
export const getGridFailureRadiiLoaded = createSelector(selectGridFailureRadii, fromGridFailureRadii.getLoaded);
export const getGridFailureRadiiLoading = createSelector(selectGridFailureRadii, fromGridFailureRadii.getLoading);
export const getGridFailureRadiiFailed = createSelector(selectGridFailureRadii, fromGridFailureRadii.getFailed);
export const getGridFailureRadiiData = createSelector(selectGridFailureRadii, fromGridFailureRadii.getData);

// GridFailure expected reasons
export const selectGridFailureExpectedReasons = createSelector(selectGridFailuresState, (state: GridFailureState) => state.gridFailureExpectedReasons);
export const getGridFailuresExpectedReasonsLoaded = createSelector(selectGridFailureExpectedReasons, fromGridFailureExpectedReasons.getLoaded);
export const getGridFailureExpectedReasonsLoading = createSelector(selectGridFailureExpectedReasons, fromGridFailureExpectedReasons.getLoading);
export const getGridFailureExpectedReasonsFailed = createSelector(selectGridFailureExpectedReasons, fromGridFailureExpectedReasons.getFailed);
export const getGridFailureExpectedReasonsData = createSelector(selectGridFailureExpectedReasons, fromGridFailureExpectedReasons.getData);

// Stations
export const selectStations = createSelector(selectGridFailuresState, (state: GridFailureState) => state.stations);
export const getStationsLoaded = createSelector(selectStations, fromStations.getLoaded);
export const getStationsLoading = createSelector(selectStations, fromStations.getLoading);
export const getStationsFailed = createSelector(selectStations, fromStations.getFailed);
export const getStationsData = createSelector(selectStations, fromStations.getData);

// GridFailure stations
export const selectGridFailureStations = createSelector(selectGridFailuresState, (state: GridFailureState) => state.gridFailureStations);
export const getGridFailureStationsLoaded = createSelector(selectGridFailureStations, fromGridFailureStations.getLoaded);
export const getGridFailureStationsLoading = createSelector(selectGridFailureStations, fromGridFailureStations.getLoading);
export const getGridFailureStationsFailed = createSelector(selectGridFailureStations, fromGridFailureStations.getFailed);
export const getGridFailureStationsData = createSelector(selectGridFailureStations, fromGridFailureStations.getData);

// HistGridFailure stations
export const selectHistGridFailureStations = createSelector(selectGridFailuresState, (state: GridFailureState) => state.histGridFailureStations);
export const getHistGridFailureStationsLoaded = createSelector(selectHistGridFailureStations, fromHistGridFailureStations.getLoaded);
export const getHistGridFailureStationsLoading = createSelector(selectHistGridFailureStations, fromHistGridFailureStations.getLoading);
export const getHistGridFailureStationsFailed = createSelector(selectHistGridFailureStations, fromHistGridFailureStations.getFailed);
export const getHistGridFailureStationsData = createSelector(selectHistGridFailureStations, fromHistGridFailureStations.getData);

// GridFailure polygon
export const selectGridFailurePolygon = createSelector(selectGridFailuresState, (state: GridFailureState) => state.gridFailurePolygon);
export const getGridFailurePolygonLoaded = createSelector(selectGridFailurePolygon, fromGridFailurePolygon.getLoaded);
export const getGridFailurePolygonLoading = createSelector(selectGridFailurePolygon, fromGridFailurePolygon.getLoading);
export const getGridFailurePolygonFailed = createSelector(selectGridFailurePolygon, fromGridFailurePolygon.getFailed);
export const getGridFailurePolygonData = createSelector(selectGridFailurePolygon, fromGridFailurePolygon.getData);

// CondensedGridFailures
export const selectCondensedGridFailures = createSelector(selectGridFailuresState, (state: GridFailureState) => state.condensedGridFailures);
export const getCondensedGridFailuresLoaded = createSelector(selectCondensedGridFailures, fromCondensedGridFailures.getLoaded);
export const getCondensedGridFailuresLoading = createSelector(selectCondensedGridFailures, fromCondensedGridFailures.getLoading);
export const getCondensedGridFailuresFailed = createSelector(selectCondensedGridFailures, fromCondensedGridFailures.getFailed);
export const getCondensedGridFailuresData = createSelector(selectCondensedGridFailures, fromCondensedGridFailures.getData);

// GridFailure reminder
export const selectGridFailureReminder = createSelector(selectGridFailuresState, (state: GridFailureState) => state.gridFailureReminder);
export const getGridFailureReminderLoaded = createSelector(selectGridFailureReminder, fromGridFailureReminder.getLoaded);
export const getGridFailureReminderLoading = createSelector(selectGridFailureReminder, fromGridFailureReminder.getLoading);
export const getGridFailureReminderFailed = createSelector(selectGridFailureReminder, fromGridFailureReminder.getFailed);
export const getGridFailureReminderData = createSelector(selectGridFailureReminder, fromGridFailureReminder.getData);

// GridFailureDistributionGroups
export const selectGridFailureDistributionGroups = createSelector(selectGridFailuresState, (state: GridFailureState) => state.gridFailureDistributionGroups);
export const getGridFailureDistributionGroupsLoaded = createSelector(selectGridFailureDistributionGroups, fromGridFailureDistributionGroups.getLoaded);
export const getGridFailureDistributionGroupsLoading = createSelector(selectGridFailureDistributionGroups, fromGridFailureDistributionGroups.getLoading);
export const getGridFailureDistributionGroupsFailed = createSelector(selectGridFailureDistributionGroups, fromGridFailureDistributionGroups.getFailed);
export const getGridFailureDistributionGroupsData = createSelector(selectGridFailureDistributionGroups, fromGridFailureDistributionGroups.getData);

// GridFailurePublicationChannels
export const selectGridFailurePublicationChannels = createSelector(selectGridFailuresState, (state: GridFailureState) => state.gridFailurePublicationChannels);
export const getGridFailurePublicationChannelsLoaded = createSelector(selectGridFailurePublicationChannels, fromGridFailurePublicationChannels.getLoaded);
export const getGridFailurePublicationChannelsLoading = createSelector(selectGridFailurePublicationChannels, fromGridFailurePublicationChannels.getLoading);
export const getGridFailurePublicationChannelsFailed = createSelector(selectGridFailurePublicationChannels, fromGridFailurePublicationChannels.getFailed);
export const getGridFailurePublicationChannelsData = createSelector(selectGridFailurePublicationChannels, fromGridFailurePublicationChannels.getData);

/**
 * DistributionGroups store functions
 */
export const selectDistributionGroupsState = createFeatureSelector<DistributionGroupState>('distribution-group');
// DistributionGroups list
export const selectDistributionGroups = createSelector(selectDistributionGroupsState, (state: DistributionGroupState) => state.distributionGroups);
export const getDistributionGroupsLoaded = createSelector(selectDistributionGroups, fromDistributionGroups.getLoaded);
export const getDistributionGroupsLoading = createSelector(selectDistributionGroups, fromDistributionGroups.getLoading);
export const getDistributionGroupsFailed = createSelector(selectDistributionGroups, fromDistributionGroups.getFailed);
export const getDistributionGroupsData = createSelector(selectDistributionGroups, fromDistributionGroups.getData);
// DistributionGroups details
export const selectDistributionGroupsDetails = createSelector(
  selectDistributionGroupsState,
  (state: DistributionGroupState) => state.distributionGroupDetailForm
);
export const getDistributionGroupsDetails = createSelector(selectDistributionGroupsDetails, fromDistributionGroupsDetailForm.getFormState);
// DistributionGroupMembers list
export const selectDistributionGroupMembers = createSelector(selectDistributionGroupsState, (state: DistributionGroupState) => state.distributionGroupMembers);
export const getDistributionGroupMembersLoaded = createSelector(selectDistributionGroupMembers, fromDistributionGroupMembers.getLoaded);
export const getDistributionGroupMembersLoading = createSelector(selectDistributionGroupMembers, fromDistributionGroupMembers.getLoading);
export const getDistributionGroupMembersFailed = createSelector(selectDistributionGroupMembers, fromDistributionGroupMembers.getFailed);
export const getDistributionGroupMembersData = createSelector(selectDistributionGroupMembers, fromDistributionGroupMembers.getData);
