/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/map';
import * as settingsActions from '@grid-failure-information-app/shared/store/actions/settings.action';
import { createReducer, on, Action } from '@ngrx/store';
import { User } from '@grid-failure-information-app/shared/models/user';
import { Settings } from '@grid-failure-information-app/shared/models';
import { InitialEmailContent } from '@grid-failure-information-app/shared/models/settings-initial-email-content.model';

export interface State {
  selectedLanguage: string;
  selectedCulture: string;
  availableLanguages: Array<any>;
  user: User;
  preConfiguration: {
    loading: boolean;
    loaded: boolean;
    failed: boolean;
    data: Settings;
  };
  initialEmailContent: {
    loading: boolean;
    loaded: boolean;
    failed: boolean;
    data: InitialEmailContent;
  };
}

export const INITIAL_STATE: State = {
  selectedLanguage: '',
  selectedCulture: '',
  availableLanguages: [{ code: 'de', name: 'DE', culture: 'de-DE' }],
  user: new User(),
  preConfiguration: {
    loading: false,
    loaded: false,
    failed: false,
    data: null,
  },
  initialEmailContent: {
    loading: false,
    loaded: false,
    failed: false,
    data: null,
  },
};

export const settingsReducer = createReducer(
  INITIAL_STATE,
  on(settingsActions.setLanguage, (state: any, action: any) => {
    return { ...state, ...{ selectedLanguage: action['payload'] } };
  }),
  on(settingsActions.setCulture, (state: any, action: any) => {
    return { ...state, selectedCulture: action['payload'] };
  }),
  on(settingsActions.setUser, (state: any, action: any) => {
    return { ...state, user: action['payload'] };
  }),
  on(settingsActions.loadPreConfiguration, (state: any, action: any) => {
    return {
      ...state,
      preConfiguration: {
        loading: true,
        loaded: false,
        failed: false,
        data: null,
      },
    };
  }),
  on(settingsActions.loadPreConfigurationSuccess, (state: any, action: any) => {
    return {
      ...state,
      preConfiguration: {
        loading: false,
        loaded: true,
        failed: false,
        data: action['payload'],
      },
    };
  }),
  on(settingsActions.loadPreConfigurationFail, (state: any, action: any) => {
    return {
      ...state,
      preConfiguration: {
        loaded: false,
        loading: false,
        failed: true,
        data: [],
      },
    };
  }),
  on(settingsActions.loadInitialEmailContent, (state: any, action: any) => {
    return {
      ...state,
      initialEmailContent: {
        loading: true,
        loaded: false,
        failed: false,
        data: null,
      },
    };
  }),
  on(settingsActions.loadInitialEmailContentSuccess, (state: any, action: any) => {
    return {
      ...state,
      initialEmailContent: {
        loading: false,
        loaded: true,
        failed: false,
        data: action['payload'],
      },
    };
  }),
  on(settingsActions.loadInitialEmailContentFail, (state: any, action: any) => {
    return {
      ...state,
      initialEmailContent: {
        loaded: false,
        loading: false,
        failed: true,
        data: [],
      },
    };
  })
);

export function reducer(state: State = INITIAL_STATE, action: Action): State {
  return settingsReducer(state, action);
}

export const getSelectedLanguage = (state: State) => state.selectedLanguage;
export const getSelectedCulture = (state: State) => state.selectedCulture;
export const getAvailableLanguages = (state: State) => state.availableLanguages;
export const getUser = (state: State) => state.user;
export const getPreConfiguration = (state: State) => state.preConfiguration.data;
export const getInitialEmailContent = (state: State) => state.initialEmailContent.data;
export const getExportChannels = (state: State) => state.preConfiguration.data.exportChannels;
