/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { FailureHousenumber, GridFailure, FailureStation, FailureBranch } from '@grid-failure-information-app/shared/models';
import * as gridFailureActions from '@grid-failure-information-app/shared/store/actions/grid-failures.action';
import { getDependencyPropertyFromControlId } from '@grid-failure-information-app/shared/utility';
import { Action, ActionReducer, INIT, UPDATE } from '@ngrx/store';
import {
  createFormGroupState,
  createFormStateReducerWithUpdate,
  disable,
  enable,
  FormGroupState,
  FormState,
  setUserDefinedProperty,
  setValue,
  SetValueAction,
  updateGroup,
  validate,
  box,
  FormGroupControls,
} from 'ngrx-forms';
import { required } from 'ngrx-forms/validation';
import { unbox } from 'ngrx-forms';
export const FORM_ID = 'gridFailureDetailsForm';
export const DEPENDENT_FIELD_KEY = 'controlId';
export const INITIAL_STATE: FormGroupState<GridFailure> = createFormGroupState<GridFailure>(FORM_ID, new GridFailure());
export const FORM_CONTROLS: FormGroupControls<GridFailure> = INITIAL_STATE.controls;
export const NO_BRANCH_ID_KEY: string = 'NO_BRANCH_ID';
export const GAS_BRANCH_ID_KEY: string = 'GAS_BRANCH_ID';
export const POWER_BRANCH_ID_KEY: string = 'POWER_BRANCH_ID';

/**
 * Helper method checks if one of dependet fields (field nameIds passed as controlNames:string[]) is set to null.
 * If so, it sets the current property value to null otherwise it reutrns the current property state
 *
 * @author Martin Gardyan <martin.gardyan@pta.de>
 * @export
 * @param {*} propState
 * @param {*} formState
 * @param {string[]} controlIds
 * @returns {any}
 */
export function setDependentPropertyValueNull(propState: any, formState: any, controlIds: string[]): any {
  const controlChanged: boolean = controlIds.some((controlId: string) => {
    return formState.userDefinedProperties[DEPENDENT_FIELD_KEY] === controlId;
  });

  if (controlChanged) {
    return setValue(propState, null);
  }

  return propState;
}

/**
 * Current ngrx-form version doesn't support validation with enable/disable
 *
 * @param state
 * @param action
 * @param controlId
 */
export function setControlEditState(propState: any, formState: FormState<any>, controlIds: string[]): any {
  const dependentControlChanged: boolean = controlIds.some((controlId: string) => {
    const controlName: string = getDependencyPropertyFromControlId(controlId);
    return !formState['controls'][controlName] || !formState['controls'][controlName]['value'];
  });

  if (formState.isDisabled || dependentControlChanged) {
    return disable(propState);
  }

  return enable(propState);
}

/**
 * Checks wheather the succes respones has an array with only one item and sets the value in form model.
 * If not it chekcs additionally, whether the (imported) control value is avaiable in the list values (action payload).
 * If not, the control value is set to null (so the user has to choose a correct value manually)
 *
 * @author Martin Gardyan <martin.gardyan@pta.de>
 * @export
 * @param {FormGroupState<GridFailure>} [state=INITIAL_STATE]
 * @param {Action} action
 * @param {string} controlId
 * @returns {FormGroupState<GridFailure>}
 */
export function getSingleValue(state: FormGroupState<GridFailure> = INITIAL_STATE, action: Action, controlId: string): FormGroupState<GridFailure> {
  const data: Array<string> = <Array<string>>action['payload'];

  const controlName: string = controlId.substring(controlId.indexOf('.') + 1, controlId.length);
  const isSingleValue: boolean = !!data && data.length <= 1 && data[0] !== state.controls[controlName].value;
  let currentAction: Action = isSingleValue
    ? new SetValueAction<any>(controlId, data[0] || null)
    : new SetValueAction<any>(controlId, data.find(d => d === state.controls[controlName].value) || null);

  let formState = setUserDefinedProperty(state, DEPENDENT_FIELD_KEY, isSingleValue ? controlId : null);
  return validateForm(formState, currentAction);
}

export function notRequiredIf(propState: any, formState: any, controlId: string, dependencyPropertyKey: string): any {
  const dependencyProperty: string = getDependencyPropertyFromControlId(controlId);
  if (formState.controls[dependencyProperty].value !== formState.userDefinedProperties[dependencyPropertyKey]) {
    return validate(propState, required);
  }
  return validate(propState, item => null);
}

// Required if dependency property is dependencyPropertyKey or null (default behavior)
export function requiredIf(propState: any, formState: any, controlId: string, dependencyPropertyKey: string): any {
  const dependencyProperty: string = getDependencyPropertyFromControlId(controlId);
  if (
    formState.controls[dependencyProperty].value === formState.userDefinedProperties[dependencyPropertyKey] ||
    formState.controls[dependencyProperty].value === null
  ) {
    return validate(propState, required);
  }
  return validate(propState, item => null);
}

export const validateForm: ActionReducer<FormState<GridFailure>> = createFormStateReducerWithUpdate<GridFailure>(
  updateGroup<GridFailure>(
    {
      city: (propState, formState): any => {
        return setDependentPropertyValueNull(propState, formState, [FORM_CONTROLS.__formBranch.id]);
      },
      postcode: (propState, formState): any => {
        return setDependentPropertyValueNull(propState, formState, [FORM_CONTROLS.__formBranch.id, FORM_CONTROLS.district.id, FORM_CONTROLS.city.id]);
      },
      district: (propState, formState): any => {
        return setDependentPropertyValueNull(propState, formState, [FORM_CONTROLS.__formBranch.id, FORM_CONTROLS.city.id]);
      },
      street: (propState, formState): any => {
        return setDependentPropertyValueNull(propState, formState, [
          FORM_CONTROLS.__formBranch.id,
          FORM_CONTROLS.district.id,
          FORM_CONTROLS.city.id,
          FORM_CONTROLS.postcode.id,
        ]);
      },
      housenumber: (propState, formState): any => {
        return setDependentPropertyValueNull(propState, formState, [
          FORM_CONTROLS.__formBranch.id,
          FORM_CONTROLS.street.id,
          FORM_CONTROLS.district.id,
          FORM_CONTROLS.city.id,
          FORM_CONTROLS.postcode.id,
        ]);
      },
      stationDescription: (propState, formState): any => {
        return setDependentPropertyValueNull(propState, formState, [FORM_CONTROLS.__formBranch.id]);
      },
      radiusId: (propState, formState): any => {
        return setDependentPropertyValueNull(propState, formState, [FORM_CONTROLS.__formBranch.id]);
      },
      radius: (propState, formState): any => {
        return setDependentPropertyValueNull(propState, formState, [FORM_CONTROLS.__formBranch.id]);
      },
      longitude: (propState, formState): any => {
        return setDependentPropertyValueNull(propState, formState, [
          FORM_CONTROLS.__formBranch.id,
          FORM_CONTROLS.city.id,
          FORM_CONTROLS.district.id,
          FORM_CONTROLS.postcode.id,
          FORM_CONTROLS.street.id,
        ]);
      },
      latitude: (propState, formState): any => {
        return setDependentPropertyValueNull(propState, formState, [
          FORM_CONTROLS.__formBranch.id,
          FORM_CONTROLS.city.id,
          FORM_CONTROLS.district.id,
          FORM_CONTROLS.postcode.id,
          FORM_CONTROLS.street.id,
        ]);
      },
      expectedReasonId: (propState, formState): any => {
        return setDependentPropertyValueNull(propState, formState, [FORM_CONTROLS.__formBranch.id]);
      },
    },
    {
      district: (propState, formState): any => {
        return setControlEditState(propState, formState, [FORM_CONTROLS.city.id]);
      },
      postcode: (propState, formState): any => {
        return setControlEditState(propState, formState, [FORM_CONTROLS.district.id]);
      },
      street: (propState, formState): any => {
        return setControlEditState(propState, formState, [FORM_CONTROLS.postcode.id]);
      },
      housenumber: (propState, formState): any => {
        return setControlEditState(propState, formState, [FORM_CONTROLS.street.id]);
      },
    },
    {
      postcode: (propState, formState): any => {
        return notRequiredIf(propState, formState, formState.controls.branchId.id, NO_BRANCH_ID_KEY);
      },
      city: (propState, formState): any => {
        return notRequiredIf(propState, formState, formState.controls.branchId.id, NO_BRANCH_ID_KEY);
      },
      street: (propState, formState): any => {
        return notRequiredIf(propState, formState, formState.controls.branchId.id, NO_BRANCH_ID_KEY);
      },
      latitude: (propState, formState): any => {
        return notRequiredIf(propState, formState, formState.controls.branchId.id, NO_BRANCH_ID_KEY);
      },
      longitude: (propState, formState): any => {
        return notRequiredIf(propState, formState, formState.controls.branchId.id, NO_BRANCH_ID_KEY);
      },
      district: (propState, formState): any => {
        return notRequiredIf(propState, formState, formState.controls.branchId.id, NO_BRANCH_ID_KEY);
      },
      housenumber: (propState, formState): any => {
        return notRequiredIf(propState, formState, formState.controls.branchId.id, NO_BRANCH_ID_KEY);
      },
      radiusId: (propState, formState): any => {
        return notRequiredIf(propState, formState, formState.controls.branchId.id, NO_BRANCH_ID_KEY);
      },
      voltageLevel: (propState, formState): any => {
        return requiredIf(propState, formState, formState.controls.branchId.id, POWER_BRANCH_ID_KEY);
      },
      pressureLevel: (propState, formState): any => {
        return requiredIf(propState, formState, formState.controls.branchId.id, GAS_BRANCH_ID_KEY);
      },
    },
    {
      __formBranch: (propState, formState): any => {
        return !!formState.controls.branchId.value ? propState : setValue(propState, box(null));
      },
    },
    {
      branchId: validate(required),
      failureBegin: validate(required),
      expectedReasonId: validate(required),
      __formBranch: validate(required),
    }
  )
);

export function reducer(state: FormGroupState<GridFailure> = INITIAL_STATE, action: Action): FormGroupState<GridFailure> {
  if (!action || action.type === INIT || action.type === UPDATE) {
    const setValueAction: SetValueAction<any> = new SetValueAction<any>(FORM_ID, new GridFailure());
    return validateForm(INITIAL_STATE, setValueAction);
  }
  switch (action.type) {
    case gridFailureActions.loadGridFailureVersionSuccess.type:
    case gridFailureActions.loadGridFailureDetailSuccess.type: {
      const gridFailure: GridFailure = new GridFailure(action['payload']);
      const setValueAction: SetValueAction<any> = new SetValueAction<any>(FORM_ID, state.isDisabled ? gridFailure : action['payload']);
      const formState = validateForm(state, setValueAction);

      return setUserDefinedProperty(formState, DEPENDENT_FIELD_KEY, FORM_ID);
    }
    case gridFailureActions.loadAllAddressCommunitiesSuccess.type: {
      return getSingleValue(state, action, FORM_CONTROLS.city.id);
    }
    case gridFailureActions.loadAddressPostalcodesSuccess.type: {
      return getSingleValue(state, action, FORM_CONTROLS.postcode.id);
    }
    case gridFailureActions.loadAddressDistrictsOfCommunitySuccess.type: {
      return getSingleValue(state, action, FORM_CONTROLS.district.id);
    }
    case gridFailureActions.loadAddressStreetsSuccess.type: {
      return getSingleValue(state, action, FORM_CONTROLS.street.id);
    }
    case gridFailureActions.loadAddressHouseNumbersSuccess.type: {
      const ac = { payload: !!action['payload'] && action['payload'].map((houseNumber: FailureHousenumber) => houseNumber.housenumber) } as any;
      return getSingleValue(state, ac, FORM_CONTROLS.housenumber.id);
    }
    case SetValueAction.TYPE: {
      let formState = setUserDefinedProperty(state, DEPENDENT_FIELD_KEY, action[DEPENDENT_FIELD_KEY]);
      if (action['controlId'] === INITIAL_STATE.controls.__formBranch.id) {
        const branch = unbox(action['value']) as FailureBranch;

        return validateForm(
          formState,
          new SetValueAction(FORM_ID, {
            ...formState.value,
            branchId: branch.id,
            branch: branch.name,
            branchDescription: branch.description,
            postcode: null,
            __formBranch: box(JSON.stringify(branch)),
          })
        );
      }

      return validateForm(formState, action);
    }
    default:
      return validateForm(state, action);
  }
}

export const getFormState = (state: FormGroupState<GridFailure>) => state;
