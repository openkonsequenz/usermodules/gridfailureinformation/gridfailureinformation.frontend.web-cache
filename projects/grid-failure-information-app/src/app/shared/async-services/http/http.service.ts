/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { Http, Request } from '@angular/http';
import { ConfigService } from '@grid-failure-information-app/app/app-config.service';
import { HttpAdapter } from '@grid-failure-information-app/shared/async-services/http/http.adapter';
import { HttpResponseHandler } from '@grid-failure-information-app/shared/async-services/http/httpResponseHandler.service';
import { Observable } from 'rxjs';

/**
 * Supported @Produces media types
 */
export enum MediaType {
  JSON,
  FORM_DATA,
}

@Injectable()
export class HttpService {
  public constructor(protected http: Http, protected configService: ConfigService, protected responseHandler: HttpResponseHandler) {}

  protected getBaseUrl(): string {
    return this.configService.get('api').baseUrl;
  }

  protected getDefaultHeaders(): Object {
    return null;
  }

  /**
   * Request Interceptor
   *
   * @method requestInterceptor
   * @param {Request} req - request object
   */
  protected requestInterceptor(req: Request) {}

  /**
   * Response Interceptor
   *
   * @method responseInterceptor
   * @param {Response} observableRes - response object
   * @returns {Response} res - transformed response object
   */
  protected responseInterceptor(observableRes: Observable<any>, adapterFn?: Function): Observable<any> {
    return observableRes.map(res => HttpAdapter.baseAdapter(res, adapterFn)).catch((err, source) => this.responseHandler.onCatch(err, source));
  }
}
