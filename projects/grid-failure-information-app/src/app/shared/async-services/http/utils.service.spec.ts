/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import * as utils from '@grid-failure-information-app/shared/async-services/http/utils.service';

describe('UtilsService', () => {
  it('should call methodBuilder and return a descriptor', () => {
    const propertyKey = 'test_property';
    const target: any = {
      test_property_Path_parameters: 'test_path',
      test_property_Query_parameters: 'test_query',
      test_property_Body_parameters: 'test_body',
      test_property_Header_parameters: 'test_header',
    };

    const descriptor = { value: () => {}, adapter: {} };

    // 0->GET
    const methodObject = utils.methodBuilder(0)('/testurl')(target, propertyKey, descriptor);

    expect(methodObject).toBe(descriptor);
    expect(typeof methodObject.value === 'function').toBe(true);
  });

  it('should call paramBuilder and return a descriptor', () => {
    const paramName = 'test_param';
    const key = 'test_key';
    const propertyKey = 'test_property';
    const parameterIndex = 3;

    const target: any = {
      test_property_Path_parameters: 'test_path',
      test_property_Query_parameters: 'test_query',
      test_property_Body_parameters: 'test_body',
      test_property_Header_parameters: 'test_header',
    };

    utils.paramBuilder(paramName)(key)(target, propertyKey, parameterIndex);
    expect(target).toBeDefined(); //dummy expect
  });
});
