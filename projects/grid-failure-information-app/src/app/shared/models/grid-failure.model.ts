/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Globals } from '@grid-failure-information-app/shared/constants/globals';
import { box, Boxed } from 'ngrx-forms';
import { FailureBranch } from './failure-branch.model';

export class GridFailureForm {
  public __formBranch: Boxed<string> = null as any;
  public constructor(data: GridFailure = null) {
    if (!data) {
      return;
    }
    const branch = { id: data.branchId, name: data.branch, description: data.branchDescription, colorCode: data.branchColorCode } as FailureBranch;
    this.__formBranch = box(JSON.stringify(branch));
  }
}
export class GridFailure extends GridFailureForm {
  public id: string = null;
  public branch: string = null;
  public branchColorCode: string = null;
  public branchDescription: string = null;
  public branchId: any = null;
  public condensed: boolean = null;
  public condensedCount: number = null;
  public city: string = null;
  public district: string = null;
  public failureBegin: string = null;
  public failureClassification: string = null;
  public failureClassificationId: string = null;
  public failureEndPlanned: string = null;
  public failureEndResupplied: string = null;
  public failureType: string = null;
  public freetextCity: string = null;
  public freetextDistrict: string = null;
  public freetextPostcode: string = null;
  public housenumber: string = null;
  public internalRemark: string = null;
  public postcode: string = null;
  public pressureLevel: string = null;
  public radius: number = null;
  public radiusId: string = null;
  public responsibility: string = null;
  public stationId: string = null;
  public stationDescription: string = null;
  public stationIds: Array<string> | Boxed<Array<string>> = null;
  public statusExtern: string = null;
  public statusExternId: string = null;
  public statusIntern: string = null;
  public statusInternId: string = null;
  public street: string = null;
  public voltageLevel: string = null;
  public createDate: string = null;
  public createUser: string = null;
  public modDate: string = null;
  public modUser: string = null;
  public objectReferenceExternalSystem: string = null;
  public publicationStatus: string = null;
  public publicationFreetext: string = null;
  public expectedReasonText: string = null;
  public expectedReasonId: string = null;
  public longitude: number = null;
  public latitude: number = null;
  public versionNumber: number = 0;
  public failureInformationCondensedId: string = null;
  public addressPolygonPoints: Array<[Number, Number]> | Boxed<Array<[Number, Number]>> = null;
  public description: string = null;
  public faultLocationArea: string = null;

  public constructor(data: any = null) {
    super(data);
    Object.keys(data || {})
      .filter(property => this.hasOwnProperty(property))
      .forEach(property => {
        if (Globals.PROPERTIES_TO_BOX.includes(property)) {
          this[property] = box(data[property]);
        } else {
          this[property] = data[property];
        }
      });
  }
}
