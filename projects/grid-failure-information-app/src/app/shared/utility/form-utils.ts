/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { unbox } from 'ngrx-forms';

/**
 * Unboxes all boxed elements of the passed object.
 * Properties to unbox are identified by the '__boxed' property.
 */
export function unboxProperties<T>(obj: any): T {
  Object.keys(obj || {})
    .filter(property => obj.hasOwnProperty(property))
    .forEach(property => {
      if (obj[property] && obj[property]['__boxed'] !== undefined) {
        obj[property] = unbox(obj[property]);
      }
    });

  return obj as T;
}
