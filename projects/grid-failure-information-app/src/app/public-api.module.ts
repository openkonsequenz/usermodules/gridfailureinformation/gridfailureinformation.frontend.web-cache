import { ComponentsModule } from './shared/components/components.module';
/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DateCellRendererComponent } from '@grid-failure-information-app/shared/components/cell-renderer/date-cell-renderer/date-cell-renderer.component';
import { DateTimeCellRendererComponent } from '@grid-failure-information-app/shared/components/cell-renderer/date-time-cell-renderer/date-time-cell-renderer.component';
import { SpinnerComponent } from '@grid-failure-information-app/shared/components/spinner/spinner.component';
import { AutoResizeColumnsDirective } from '@grid-failure-information-app/shared/directives/agGrid/auto-resize-columns.directive';
import { DirectivesModule } from './shared/directives/directives.module';

export const COMPONENTS = [SpinnerComponent, DateCellRendererComponent, DateTimeCellRendererComponent, AutoResizeColumnsDirective];

@NgModule({
  imports: [CommonModule, DirectivesModule, ComponentsModule],
  exports: COMPONENTS,
  entryComponents: [DateCellRendererComponent, DateTimeCellRendererComponent],
})
// functionality needed for grid-failure-information-table-app
export class PublicApiModule {}
