/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { TableComponent } from '@grid-failure-information-table-app/app/app.component';
import { AppTableService } from '@grid-failure-information-table-app/app/app-table.service';
import { AppConfigService } from '@grid-failure-information-table-app/app/app-config.service';
import { GridFailure, Settings } from '@grid-failure-information-app/shared/models';
import { of } from 'rxjs';
import { DatePipe } from '@angular/common';
import { VisibilityEnum } from '@grid-failure-information-app/shared/constants/enums';

describe('AppComponent ', () => {
  let component: TableComponent;
  let service: AppTableService;
  let configService: AppConfigService;
  let gridFailureMapListAll: GridFailure[] = [];
  let datePipe: DatePipe;

  beforeEach(() => {
    service = {
      loadGridFailureData: () => of([new GridFailure()]),
    } as any;
    configService = {
      getConfig: () => of(true),
    } as any;
    datePipe = { transform: () => '' } as any;
    gridFailureMapListAll = [
      new GridFailure({ postcode: '007', modDate: '2020-08-13T13:35:44.808Z' }),
      new GridFailure({ postcode: '4711', modDate: '2020-08-14T13:35:44.808Z' }),
      new GridFailure({ postcode: '0815', modDate: '2019-08-13T13:35:44.808Z' }),
    ];
    component = new TableComponent(service, configService, datePipe);
    (component as any)._gridFailuresAll = gridFailureMapListAll;
    component.gridFailures = gridFailureMapListAll.map(i => Object.assign(i));
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should call loadGridFailureData', () => {
    const spy: any = spyOn(component['_appTableService'], 'loadGridFailureData').and.returnValue(of({} as any));
    component.ngOnInit();
    expect(spy).toHaveBeenCalled();
  });

  it('should set initial sorting', () => {
    let sortModel;
    let params = { api: { setSortModel: sortModel = {} } };
    component['_gridApi'] = params.api;
    component['_datePipe'] = { transform: () => '18.09.2020 / 10:17' } as any;
    const spy: any = spyOn(component['_gridApi'], 'setSortModel');
    component.onGridReady(params);
    expect(spy).toHaveBeenCalled();
  });

  it('should assign all gridfailures to table', () => {
    let config = new Settings();
    config.dataExternInitialVisibility = VisibilityEnum.SHOW;
    configService = {
      getConfig: () => of(config),
    } as any;
    component = new TableComponent(service, configService, null);
    component.postcode = '';
    component['_datePipe'] = { transform: () => '18.09.2020 / 10:17' } as any;
    component.ngOnInit();
    expect(component.gridFailures.length).toEqual(1);
  });

  it('should assign only gridfailures with postcode 007 to table', () => {
    component.postcode = '007';
    expect(component.gridFailures.length).toEqual(1);
    expect(component.gridFailures[0].postcode).toEqual('007');
  });

  it('should get the last modDate of the GridFailure array after "_getLastModeDate"', () => {
    const lastTimeStamp = (component as any)._getLastModeDate();
    expect(lastTimeStamp).toEqual(1597412144808); // == '2020-08-14T13:35:44.808Z'
  });

  it('should define columnDefs in case config is provided', () => {
    let config = new Settings();
    config.visibilityConfiguration = { tableExternColumnVisibility: { failureClassification: 'show' } } as any;
    configService = {
      getConfig: () => of(config),
    } as any;
    component = new TableComponent(service, configService, null);
    component.ngOnInit();
    expect(component.columnDefs).toBeDefined();
  });
});
