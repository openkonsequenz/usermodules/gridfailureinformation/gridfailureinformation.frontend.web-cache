/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { AppConfigService } from '@grid-failure-information-table-app/app/app-config.service';

describe('AppConfigService', () => {
  let service: AppConfigService;
  let mockHttpClient;

  beforeEach(() => {
    mockHttpClient = { get: () => {} };
    service = new AppConfigService(mockHttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call http het via getConfig()', () => {
    const spy: any = spyOn(service['http'], 'get');
    service.getConfig();
    expect(spy).toHaveBeenCalledWith('public-settings');
  });
});
